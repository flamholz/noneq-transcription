function [delta,pONVec] = hmCalcHelperVec(RNum,activeStates,nStates)

    n_trials = size(RNum,3);
    r_cell = cell(1,n_trials);
    for n = 1:n_trials
      r_cell{n} = RNum(:,:,n);
    end
    [V,D] = cellfun(@eig,r_cell,'UniformOutput',false);
    D_cat = cat(3,D{:});
    V_cat = cat(3,V{:});

    % use linear indexing to extract eigenvalues
    depth_index = reshape((0:n_trials-1)*nStates^2,1,1,[]);
    diag_indices = (0:nStates-1)*nStates + (1:nStates) + depth_index;
    D_diag = D_cat(diag_indices);
    [~,mi] = max(real(D_diag),[],2);

    % now extract appropriate V vectors for each array
    col_row_indices = nStates*(mi-1) + (1:nStates)' + depth_index;
    ss_array = V_cat(col_row_indices);
    ss_array = ss_array ./ sum(ss_array);
    
%     [V,D] = eig(RNum);
%     [~,mi] = max(real(diag(D)));
%     ss_vec_full = V(:,mi)./sum(V(:,mi));
    pONVec = sum(ss_array(activeStates,:,:),1);
%     pOFF = sum(ss_vec_full(~activeStates));
    delta = sum((pONVec - 0.5).^2);