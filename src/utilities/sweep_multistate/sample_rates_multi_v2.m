function new_params = sample_rates_multi_v2(lb_array,ub_array,orig_param_array,prop_sigma,simInfo)

  % extract key parameters
  paramBounds = simInfo.paramBounds;  
  equilibrium_flag = simInfo.equilibrium_flag;
  sweepFlags = simInfo.sweepFlags;
  
  % generate proposed rates
  if isempty(orig_param_array)
      new_params_temp = reshape(10.^(prop_sigma*trandn(lb_array,ub_array)),[],sum(sweepFlags)); % function to sample from truncated normal dist
  else
      new_params_temp = 10.^(orig_param_array(:,sweep_flags)+reshape(prop_sigma*trandn(lb_array,ub_array),[],sum(sweepFlags)));
  end   
        
  % set param values for static variables
  new_params = NaN(size(new_params_temp,1),length(sweepFlags));
  new_params(:,sweepFlags) = new_params_temp;
  new_params(:,~sweepFlags) = repmat(simInfo.defaultValues(~sweepFlags),size(new_params,1),1);
  
  if equilibrium_flag
      % NL: for now I will implement a solution that assumes symmetry
      % between right and wrong cycles. Will need to revise if I choose to
      % examine more general architectures
      
      % generate input cell 
      forward_indices = simInfo.forwardRatesRight;
      backward_indices = simInfo.backwardRatesRight;
      
      % use rejection sampling to find reverse rates
      new_rates_temp = new_params;
      err_flags = true(size(new_params,1),1);
  
      % attempt to resample reverse rates         
      flux_factor = prod(new_rates_temp(err_flags,forward_indices),2) ./ prod(new_rates_temp(err_flags,backward_indices),2);
      flux_factor = flux_factor.^(1/length(backward_indices));
      new_rates_temp(err_flags,backward_indices) = new_rates_temp(err_flags,backward_indices).*flux_factor;          

      % find cases where this renormalization pushes rates outside of bounds
      err_flags = max(log10(new_rates_temp(:,backward_indices))<paramBounds(1,backward_indices)...
        |log10(new_rates_temp(:,backward_indices))>paramBounds(2,backward_indices),[],2);

      % try the reverse operation
      new_rates_temp(err_flags,backward_indices) = new_params(err_flags,backward_indices); % reset
      new_rates_temp(err_flags,forward_indices) = new_rates_temp(err_flags,forward_indices)./flux_factor(err_flags);             

      err_flags = max(log10(new_rates_temp(:,forward_indices))<paramBounds(1,forward_indices)...
        |log10(new_rates_temp(:,forward_indices))>paramBounds(2,forward_indices),[],2);      
      
      % apply to wrong cycle
      new_params = new_rates_temp;
      if simInfo.nStates > 4
          new_params(:,simInfo.rightWrongRates(:,2)) = new_params(:,simInfo.rightWrongRates(:,1));
      end
      if any(simInfo.unbindingFlagsWrong)
          ubWrongMatchArray = simInfo.ubWrongMatchArray;
          new_params(:,ubWrongMatchArray(:,2)) = new_params(:,ubWrongMatchArray(:,1))*simInfo.specFactor;
          new_params(:,simInfo.bindingFlagsWrong) = new_params(:,simInfo.bindingFlagsWrong)*simInfo.wrongFactorConcentration;
      end
      
      new_params(err_flags,:) = NaN;
    
  end

  
  % apply half max constraint if ncessary
  if simInfo.half_max_flag
      if ~simInfo.numCalcFlag
          if simInfo.nStates == 4
              new_params = applyHMConstraintFourState_v2(simInfo,new_params);
          else
              new_params = applyHMConstraintSixState(simInfo,new_params,simInfo.rate_bounds);
          end
      end
    
      error('HM constraint not currently supported')
  end
  

  
  
  
