function [metric_vec, metric_names, metric_ub_vec, metric_lb_vec] = ...
                              calculateMetricsSym(param_array, simInfo)                            
                            
if ~isempty(param_array)       
  
    % solve for c value where production rate is at half max      
    minError = simInfo.minError; 
    
    % generate input cell     
    paramCell = mat2cell(param_array,size(param_array,1),ones(1,size(param_array,2)));
    
    % calculate production rate         
    ProductionRate = productionRateFunction(paramCell{:});
    
    % calculate sharpness     
    Sharpness = sharpnessFunction(paramCell{:})*simInfo.c_val;
          
    % calculate variance    
    Variance = intrinsicVarianceFunction(paramCell{:});
    
    % get state probabilities
    stateProbs = steadyStateVecFunction(paramCell{:});
    
    % generate pseudo four state network to calculate specificity
    if simInfo.nStates > 4              
                   
        % generate input arrays
        valArrayCS = param_array;

        % "right" factor
        valArrayC0 = valArrayCS;
        valArrayC0(:,simInfo.cr_index) = simInfo.cr0;    

        valArrayC1 = valArrayCS;
        valArrayC1(:,simInfo.cr_index) = simInfo.cr1;  
        
        % convert to cell arrays
        valCellCS = mat2cell(valArrayCS,size(valArrayCS,1),ones(1,size(valArrayCS,2)));    
        valCellC0 = mat2cell(valArrayC0,size(valArrayC0,1),ones(1,size(valArrayC0,2)));
        valCellC1 = mat2cell(valArrayC1,size(valArrayC1,1),ones(1,size(valArrayC1,2)));
        
        % right cycle 4 state        
        ProductionRateRight = n4productionRateFunction(valCellCS{:});
        ProductionRateRightC1 = n4productionRateFunction(valCellC1{:});
        ProductionRateRightC0 = n4productionRateFunction(valCellC0{:});
        ProductionRateWrong = n4WrongproductionRateFunction(valCellCS{:});
        
        SharpnessRight = (ProductionRateRightC1-ProductionRateRightC0)./(simInfo.cr1-simInfo.cr0);
        SharpnessRightNorm = SharpnessRight ./ (ProductionRateRight.*(1-ProductionRateRight));
       
        % inter-network
        specFactorAlt = log10((ProductionRateRight./ProductionRateWrong)./(simInfo.specFactor/simInfo.cw));
        deviationFactor = log10(ProductionRate./(ProductionRateRight-ProductionRate));
        
        % intra-network             
        right_flags = simInfo.n_right_bound.*simInfo.activeStateFilter;
        wrong_flags = simInfo.n_wrong_bound.*simInfo.activeStateFilter;
        right_weights = sum(right_flags.*stateProbs,2);
        wrong_weights = sum(wrong_flags.*stateProbs,2);
        specificityFactor = log10((right_weights./wrong_weights)./(simInfo.specFactor/simInfo.cw));               
        
    else
        ProductionRateRight = NaN(size(ProductionRate));
        specificityFactor = NaN(size(ProductionRate));
        specFactorAlt = NaN(size(ProductionRate));
        SharpnessRight = specificityFactor; 
        SharpnessRightNorm = specificityFactor;
    end
    
    % calculate decision metrics 
    [V,T,R] = calculateDecisionMetrics(param_array,simInfo.cr1,simInfo.cr0,minError);
    
    % calculate cycle time    
    TauOn = TauONFunction(paramCell{:});
    TauOff = TauOFFFunction(paramCell{:});
    TauCycle = TauOff+TauOn;        
    
    % "Affinity" vec
    AffinityVec = log10(TauOff./TauOn);
    
    % calculate entropy rate
    Phi = entropyRateFunction(paramCell{:}).*TauCycle;
    fe_drop = NaN(size(Phi));
    
    % simple binomial variance
    BinomialVariance = ProductionRate.*(1-ProductionRate);
    
    % generate vector to output
    metric_vec = [ProductionRate, Sharpness, fe_drop, log(sqrt(TauCycle./Variance)),...                
                  Phi.*TauCycle, TauCycle,specificityFactor,deviationFactor,ProductionRateRight,...                            
                  SharpnessRight, SharpnessRightNorm, ...
                  log(TauOn./TauOff),V.*TauCycle,1./T,R,...                  
                  log(1./(BinomialVariance)),TauOn,TauOff,specFactorAlt,AffinityVec];
        
    im_flags =  any(round(real(metric_vec),4)~=round(metric_vec,4) & ~isnan(metric_vec),2);
    metric_vec(im_flags,:) = NaN;
    metric_vec = real(metric_vec);

else
    metric_vec = [];  
end

metric_names = {'Production Rate','Sharpness','Flux','Precision',...
                'Phi', 'CycleTime','Specificity','deviationFactor','ProductionRateRight',...
                'SharpnessRight','SharpnessRightNorm','ONOFFRatio',...
                'DecisionRateNorm','DecisionTimeNorm','ProductionBinary',...
                'BinomialNoise','TauOn','TauOff','specFactorAlt','AffinityVec'};

% specify default bounds
metric_ub_vec = repelem(Inf,length(metric_names));
metric_lb_vec = repelem(-Inf,length(metric_names));
