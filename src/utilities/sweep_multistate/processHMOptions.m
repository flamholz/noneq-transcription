function simInfo = processHMOptions(simInfo)
    
    if simInfo.half_max_flag
        if simInfo.nStates == 4 && ~simInfo.numCalcFlag
            loadPath = [simInfo.functionPath 'HM_Functions' filesep];
            load([loadPath 'hmStruct.mat'],'hmStruct');
            simInfo.hmStruct = hmStruct;
        end
    end
   
    