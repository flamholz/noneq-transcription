function R_num_array = subHelperVec(val,a,subIndices,nStates)

    val(:,subIndices) = val(:,subIndices).*reshape(a,[],1);
    R_num_array = NaN(nStates,nStates,size(val,1));
    for i = 1:size(val,1)
        valCell = mat2cell(val(i,:),size(val(i,:),1),ones(1,size(val(i,:),2)));
        R_num_array(:,:,i) = RSymFun(valCell{:});
    end