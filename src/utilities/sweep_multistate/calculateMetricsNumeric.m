function [metric_vec, metric_names, metric_ub_vec, metric_lb_vec] = ...
                              calculateMetricsNumeric(param_array, simInfo)                            
                            
if ~isempty(param_array)      
  
    numerical_precision = simInfo.numerical_precision;
    
    % initialize parameter arrays
    state_probs_c0 = NaN(size(param_array,1),simInfo.nStates);  
    state_probs_cs = NaN(size(param_array,1),simInfo.nStates);  
    state_probs_c1 = NaN(size(param_array,1),simInfo.nStates);  
    state_probs_c1W = NaN(size(param_array,1),simInfo.nStates);  
    state_probs_c0W = NaN(size(param_array,1),simInfo.nStates);  
    Variance = NaN(size(param_array,1),1);
    VarianceC0 = NaN(size(param_array,1),1);
    VarianceC1 = NaN(size(param_array,1),1);
    TauCycle = NaN(size(param_array,1),1);
    TauOn = NaN(size(param_array,1),1);
    TauOff = NaN(size(param_array,1),1);
    Phi = NaN(size(param_array,1),1);
    
    % generate input arrays
    valArrayCS = param_array;
        
    % "right" factor
    valArrayC0 = valArrayCS;
    valArrayC0(:,simInfo.cr_index) = simInfo.cr0;    
    
    valArrayC1 = valArrayCS;
    valArrayC1(:,simInfo.cr_index) = simInfo.cr1;    
    
    % "wrong" factor
    valArrayC0W = valArrayCS;
    cw0 = valArrayC0W(:,simInfo.cw_index) - 0.05*valArrayC0W(:,simInfo.cw_index);
    valArrayC0W(:,simInfo.cw_index) = cw0;    
    
    valArrayC1W = valArrayCS;
    cw1 = valArrayC1W(:,simInfo.cw_index) + 0.05*valArrayC1W(:,simInfo.cw_index);
    valArrayC1W(:,simInfo.cw_index) = cw1;  
    
    % helper vec
    activeStateFilter = simInfo.activeStateFilter;
    
%     %%
%     tic
%     q_cell_cs = cell(1,size(param_array,1));
%     q_cell_c1 = cell(1,size(param_array,1));
%     q_cell_c0 = cell(1,size(param_array,1));
%     for p = 1:length(q_cell_cs)
%       vals_cs = param_array(p,:);
%       inCell_cs = mat2cell(vals_cs,size(vals_cs,1),ones(1,size(vals_cs,2)));
%       q_cell_cs{p} = RSymFun(inCell_cs{:});
%       
%       vals_c0 = valArrayC0(p,:);
%       inCell_c0 = mat2cell(vals_cs,size(vals_c0,1),ones(1,size(vals_c0,2)));
%       q_cell_c0{p} = RSymFun(inCell_c0{:});
%       
%       vals_c1 = valArrayC1(p,:);
%       inCell_c1 = mat2cell(vals_c1,size(vals_c1,1),ones(1,size(vals_c1,2)));
%       q_cell_c1{p} = RSymFun(inCell_c1{:});
%     end
%     np_cell = repelem({numerical_precision},length(q_cell_cs));
%     toc
%     tic
%     ss_cs_cell = cellfun(@calculate_ss_num,q_cell_cs,np_cell,'UniformOutput',false);
%     ss_c1_cell = cellfun(@calculate_ss_num,q_cell_c1,np_cell,'UniformOutput',false);
%     ss_c0_cell = cellfun(@calculate_ss_num,q_cell_c0,np_cell,'UniformOutput',false);
%     toc
%     
    % calculate metric values for each row
    for i = 1:size(param_array,1)
      
        valCellCS = mat2cell(valArrayCS(i,:),size(valArrayCS(i,:),1),ones(1,size(valArrayCS(i,:),2)));    
        valCellC0 = mat2cell(valArrayC0(i,:),size(valArrayC0(i,:),1),ones(1,size(valArrayC0(i,:),2)));
        valCellC1 = mat2cell(valArrayC1(i,:),size(valArrayC1(i,:),1),ones(1,size(valArrayC1(i,:),2)));
        valCellC0W = mat2cell(valArrayC0W(i,:),size(valArrayC0(i,:),1),ones(1,size(valArrayC0W(i,:),2)));
        valCellC1W = mat2cell(valArrayC1W(i,:),size(valArrayC1(i,:),1),ones(1,size(valArrayC1W(i,:),2)));
        
        % get rate arrays
        Q_num_cs = RSymFun(valCellCS{:});
        Q_num_c0 = RSymFun(valCellC0{:});
        Q_num_c1 = RSymFun(valCellC1{:});
        Q_num_c0W = RSymFun(valCellC0W{:});
        Q_num_c1W = RSymFun(valCellC1W{:});
        
        % calculate probabilities
        state_probs_cs(i,:) = calculate_ss_num(Q_num_cs,numerical_precision);        
        state_probs_c0(i,:) = calculate_ss_num(Q_num_c0,numerical_precision);
        state_probs_c1(i,:) = calculate_ss_num(Q_num_c1,numerical_precision);
        state_probs_c0W(i,:) = calculate_ss_num(Q_num_c0W,numerical_precision);
        state_probs_c1W(i,:) = calculate_ss_num(Q_num_c1W,numerical_precision);
        
        % calculate center values for variance, entropy rate, and Tau
        Z_num_cs = calculate_Z_matrix(Q_num_cs,state_probs_cs(i,:),numerical_precision);
        Variance(i) = calculate_var_num(Q_num_cs,state_probs_cs(i,:),activeStateFilter,Z_num_cs,numerical_precision);
        Phi(i) = calculate_entropy_rate_num(Q_num_cs,state_probs_cs(i,:),numerical_precision);        
        [TauOn(i),TauOff(i),TauCycle(i)] = calculate_tau_num(Q_num_cs,state_probs_cs(i,:),activeStateFilter,numerical_precision);
      
        % variance for high and low concentration
        Z_num_c0 = calculate_Z_matrix(Q_num_c0,state_probs_c0(i,:),numerical_precision);
        Z_num_c1 = calculate_Z_matrix(Q_num_c1,state_probs_c1(i,:),numerical_precision);
        VarianceC0(i) = calculate_var_num(Q_num_c0,state_probs_c0(i,:),activeStateFilter,Z_num_c0,numerical_precision);
        VarianceC1(i) = calculate_var_num(Q_num_c1,state_probs_c1(i,:),activeStateFilter,Z_num_c1,numerical_precision);
    end    

    % calculate sharpness and production rate
    ProductionRate1 = sum(state_probs_c1(:,activeStateFilter),2);
    ProductionRate0 = sum(state_probs_c0(:,activeStateFilter),2);   
    
    Sharpness = (ProductionRate1-ProductionRate0)./(simInfo.cr1-simInfo.cr0);
    ProductionRate = sum(state_probs_cs(:,activeStateFilter),2);
    
    % calculate "wrong" sharpness
    ProductionRate1W = sum(state_probs_c1W(:,activeStateFilter),2);
    ProductionRate0W = sum(state_probs_c0W(:,activeStateFilter),2); 
    pm = (ProductionRate1W + ProductionRate0W)/2;
    CWSharpness = (ProductionRate1W-ProductionRate0W)./(cw1-cw0) ./ (pm.*(1-pm));
    
    % calculate decision metrics 
    minError = simInfo.minError;
    [V,T,~] = calculateDecisionMetricsNumeric(ProductionRate0,ProductionRate1,VarianceC0,VarianceC1,minError);    
    VNorm = V.*TauCycle;

    % "Affinity" vec
    AffinityVec = log10(TauOff./TauOn);
        
    % perform calculations for "right cycle" 
    if simInfo.nStates > 4      
               
        valCellCS = mat2cell(valArrayCS,size(valArrayCS,1),ones(1,size(valArrayCS,2)));    
        valCellC0 = mat2cell(valArrayC0,size(valArrayC0,1),ones(1,size(valArrayC0,2)));
        valCellC1 = mat2cell(valArrayC1,size(valArrayC1,1),ones(1,size(valArrayC1,2)));
        
        % right cycle 4 state        
        ProductionRateRight = n4productionRateFunction(valCellCS{:});
        ProductionRateRightC1 = n4productionRateFunction(valCellC1{:});
        ProductionRateRightC0 = n4productionRateFunction(valCellC0{:});
        ProductionRateWrong = n4WrongproductionRateFunction(valCellCS{:});
        
        SharpnessRight = (ProductionRateRightC1-ProductionRateRightC0)./(simInfo.cr1-simInfo.cr0);
        SharpnessRightNorm = SharpnessRight ./ (ProductionRateRight.*(1-ProductionRateRight));
       
        % inter-network
        specFactorAlt = log10((ProductionRateRight./ProductionRateWrong)./(simInfo.specFactor/simInfo.cw));
        deviationFactor = log10(ProductionRate./(ProductionRateRight-ProductionRate));
        
        % intra-network             
        right_flags = simInfo.n_right_bound.*simInfo.activeStateFilter;
        wrong_flags = simInfo.n_wrong_bound.*simInfo.activeStateFilter;
        right_weights = sum(right_flags.*state_probs_cs,2);
        wrong_weights = sum(wrong_flags.*state_probs_cs,2);
        specificityFactor = log10((right_weights./wrong_weights)./(simInfo.specFactor/simInfo.cw)); 
    else
        specFactorAlt = NaN(size(Variance));
        specificityFactor = NaN(size(Variance));
        SharpnessRightNorm = NaN(size(Variance));
        deviationFactor = NaN(size(Variance));
    end
    
    % simple binomial noise
    BinomialVariance = ProductionRate.*(1-ProductionRate);
    
    % initialize dummy fields for now fore remaining metrics
    fe_drop = NaN(size(Variance));
        
    SharpnessRight = NaN(size(Variance));
    
    % generate vector to output
    metric_vec = [ProductionRate, Sharpness, fe_drop, log(sqrt(TauCycle./Variance)),...                
                  Phi.*TauCycle, TauCycle,specificityFactor,CWSharpness,deviationFactor,...                            
                  SharpnessRight, SharpnessRightNorm, ...
                  VNorm,TauCycle./T,V,...                 
                  log(1./(BinomialVariance)),TauOn,TauOff,specFactorAlt,AffinityVec];
                
    % deal with imaginary values
    im_flags =  any(round(real(metric_vec),4)~=round(metric_vec,4) & ~isnan(metric_vec),2);
    metric_vec(im_flags,:) = NaN;
    metric_vec = real(metric_vec);

else
    metric_vec = [];  
end

metric_names = {'Production Rate','Sharpness','Flux','Precision',...
                'Phi', 'CycleTime','Specificity','CWSharpness','deviationFactor',...
                'SharpnessRight', 'SharpnessRightNorm',...
                'DecisionRateNorm','DecisionTimeNorm',...
                'DecisionRate','BinomialNoise','TauOn','TauOff','specFactorAlt','AffinityVec'};

% specify default bounds
metric_ub_vec = repelem(Inf,length(metric_names));
metric_lb_vec = repelem(-Inf,length(metric_names));
