function RSym = RSymFun(cr,cw,b,kip,kap,kim,kam,kpi,kpa,kmi,kma,wip,wap,wma,wmi)
%RSYMFUN
%    RSYM = RSYMFUN(CR,CW,B,KIP,KAP,KIM,KAM,KPI,KPA,KMI,KMA,WIP,WAP,WMA,WMI)

%    This function was generated by the Symbolic Math Toolbox version 8.5.
%    22-Apr-2021 16:43:30

t2 = b.*kma;
t3 = b.*kmi;
t4 = cr.*kpa;
t5 = cr.*kpi;
t6 = cw.*kpa;
t7 = cw.*kpi;
t8 = kap.*wap;
t9 = kip.*wip;
t10 = -kap;
t11 = -kip;
t12 = 1.0./wma;
t13 = 1.0./wmi;
t14 = -t2;
t15 = -t3;
t16 = -t4;
t17 = -t5;
t18 = -t6;
t19 = -t7;
t20 = -t8;
t21 = -t9;
t22 = kma.*t12;
t23 = kmi.*t13;
t24 = t2.*t12;
t25 = t3.*t13;
t26 = t24.*2.0;
t27 = t25.*2.0;
t28 = -t22;
t29 = -t23;
t30 = t12.*t14;
t32 = t13.*t15;
t36 = t10+t15+t17+t19;
t37 = t11+t14+t16+t18;
t31 = -t26;
t33 = -t27;
t38 = t20+t29+t32;
t39 = t21+t28+t30;
t34 = t20+t33;
t35 = t21+t31;
RSym = reshape([-kam-t5.*2.0-t7.*2.0,t5,0.0,kam,0.0,t7,t5,0.0,0.0,0.0,0.0,0.0,t7,0.0,0.0,0.0,0.0,0.0,kmi,-kmi+t10+t17+t19,kap,0.0,0.0,0.0,0.0,t5,0.0,0.0,0.0,0.0,0.0,t7,0.0,0.0,0.0,0.0,0.0,kip,-kma+t11+t16+t18,kma,0.0,0.0,0.0,0.0,t4,0.0,0.0,0.0,0.0,0.0,t6,0.0,0.0,0.0,kim,0.0,t4,-kim-t4.*2.0-t6.*2.0,t6,0.0,0.0,0.0,0.0,t4,0.0,0.0,0.0,0.0,0.0,t6,0.0,0.0,0.0,0.0,0.0,t2,t37,kip,0.0,0.0,0.0,0.0,t4,0.0,0.0,0.0,0.0,0.0,t6,0.0,t3,0.0,0.0,0.0,kap,t36,0.0,0.0,0.0,0.0,0.0,t5,0.0,0.0,0.0,0.0,0.0,t7,t3,0.0,0.0,0.0,0.0,0.0,t36,t5,0.0,kap,0.0,t7,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t25,0.0,0.0,0.0,0.0,t23,t38,t8,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t24,0.0,0.0,0.0,0.0,t9,t39,t22,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t2,0.0,0.0,kip,0.0,t4,t37,t6,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t24,0.0,0.0,0.0,0.0,t24,t35,t9,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t25,t25,0.0,0.0,0.0,t8,t34,0.0,0.0,0.0,0.0,0.0,0.0,t3,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t36,t5,0.0,kap,0.0,t7,0.0,t25,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t23,t38,t8,0.0,0.0,0.0,0.0,0.0,t24,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t9,t39,t22,0.0,0.0,0.0,0.0,0.0,t2,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,kip,0.0,t4,t37,t6,0.0,0.0,0.0,0.0,0.0,t24,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,t24,t35,t9,0.0,0.0,0.0,0.0,0.0,t25,0.0,0.0,0.0,0.0,0.0,0.0,t25,0.0,0.0,0.0,t8,t34],[18,18]);
