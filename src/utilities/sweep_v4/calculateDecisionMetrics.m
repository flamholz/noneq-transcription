function [V,T,R] = calculateDecisionMetrics(rate_array,c1,c0,minError)

  % generate input cell arrays
  valMat1 = [c1*ones(size(rate_array,1),1) rate_array];
  valCell1 = mat2cell(valMat1,size(valMat1,1),ones(1,size(valMat1,2)));
  
  valMat0 = [c0*ones(size(rate_array,1),1) rate_array];
  valCell0 = mat2cell(valMat0,size(valMat0,1),ones(1,size(valMat0,2)));

%   % calculate cycle times
%   Ton1 = TauONFunction(valCell1{:});
%   Toff1 = TauOFFFunction(valCell1{:});
%   Tau1 = Ton1+Toff1; 
%     
%   Ton0 = TauONFunction(valCell0{:});
%   Toff0 = TauOFFFunction(valCell0{:});
%   Tau0 = Ton0+Toff0; 
%   
  % set threshold for decision    
  K = log((1-minError)./minError);

  r1 = productionRateFunction(valCell1{:});
  r0 = productionRateFunction(valCell0{:});
  
  v1 = intrinsicVarianceFunction(valCell1{:});
  v1 = v1;% ./ Tau1;
  v0 = intrinsicVarianceFunction(valCell0{:}); 
  v0 = v0;% ./ Tau0;
  
  % calculate expected drift 
  V1 = .5 .* (r0 - r1).^2 ./ v0;
  V0 = -.5 .* (r0 - r1).^2 ./ v1;
  V = .5*V1 + abs(.5*V0);
  
  % calculate diffusion
  D1 = (r0-r1).^2 .* v1 ./ (2.*v0.^2);
  D0 = (r0-r1).^2 .* v0 ./ (2.*v1.^2);
  
  % calculate decision times 
  B1 = V1.*K./D1;
  T1 = K./(2.*V1.*sinh(B1)) .* (exp(B1) + exp(-B1) - 2);
  
  B0 = -V0.*K./D0;
  T0 = -K./(2.*V0.*sinh(B0)) .* (exp(B0) + exp(-B0) - 2);
  
  T = .5*T1 + .5*T0;
  
  % calculate average occupancy
  R = 0.5*r1 + 0.5*r0;
  
%   % set threshold for decision    
%   K = log((1-minError)./minError);
% 
%   r1 = fourStateProduction_v4(rate_array,c1);
%   r0 = fourStateProduction_v4(rate_array,c0);
%   
%   v1 = fourStateVariance_v4(rate_array,c1);
%   v0 = fourStateVariance_v4(rate_array,c0);