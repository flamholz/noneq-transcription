% This script explores possibility of keeping SS vec in symbolic form
clear
close all

savePath = '../utilities/metricFunctions/n18_OR_NUM/';
mkdir(savePath);
addpath(savePath)

% define some basic parameters
activeStatesBase = [3 4 5];
baseNum = 6;
nStates = 18;
% sensingEdges = [2,1 ; 3,4];
stateIndex = 1:nStates;

% define symbolic variables
RSymFull = sym('k%d%d', [nStates nStates],'positive');

% zero out forbidden transitions
[fromRef,toRef] = meshgrid(1:baseNum,1:baseNum);
diffRef = abs(fromRef-toRef);
toRefHalved = toRef<=baseNum/2;
fromRefHalved = fromRef<=baseNum/2;
permittedConnectionsRaw= (diffRef==1 & toRefHalved==fromRefHalved) | diffRef==baseNum/2;

% permute these connections to follow a more intuitive labeling scheme
indexCurr = 1:baseNum;
indexAdjusted = circshift(indexCurr,floor(baseNum/4));
indexAdjusted = [indexAdjusted(1:baseNum/2) fliplr(indexAdjusted(baseNum/2+1:end))];
[~,si] = sort(indexAdjusted);
permittedConnectionsInit = permittedConnectionsRaw(si,si);

% generate an array with binding info
transitionInfoInit = zeros(size(permittedConnectionsInit));

% specific binding/unbinding
spec_pairs = {[1,2],[4,3]};
for s = 1:length(spec_pairs)
    ind1 = spec_pairs{s}(1);
    ind2 = spec_pairs{s}(2);
    % update
    transitionInfoInit(ind2,ind1) = 1;
    transitionInfoInit(ind1,ind2) = -1;
end

% non-specific binding/unbinding
non_spec_pairs = {[1,6],[4,5]};
for s = 1:length(non_spec_pairs)
    ind1 = non_spec_pairs{s}(1);
    ind2 = non_spec_pairs{s}(2);
    % update
    transitionInfoInit(ind2,ind1) = 2;
    transitionInfoInit(ind1,ind2) = -2;
end

% locus activity fluctuations
locus_pairs = {[1,4],[6,5],[2,3]};
for s = 1:length(locus_pairs)
    ind1 = locus_pairs{s}(1);
    ind2 = locus_pairs{s}(2);
    % update
    transitionInfoInit(ind2,ind1) = 3;
    transitionInfoInit(ind1,ind2) = -3;
end

% generate array tht indicates activity state
activity_vec_init = false(1,baseNum);
activity_vec_init(activeStatesBase) = 1;

% generate flags indicating numbers of right and wrong factors bound
n_right_bound_init = zeros(size(activity_vec_init));
n_wrong_bound_init = n_right_bound_init;
n_right_bound_init([2 3]) = 1;
n_wrong_bound_init([5 6]) = 1;

% generate full 18 state array
rate_mask = repelem(eye(3),baseNum,baseNum);

% full connection array
permittedConnections = repmat(permittedConnectionsInit,3,3);%permittedConnectionsInit
permittedConnections = permittedConnections.*rate_mask;

% full binding array
transitionInfoArray = repmat(transitionInfoInit,3,3);%permittedConnectionsInit
transitionInfoArray = transitionInfoArray.*rate_mask;

% full activity vector
activity_vec_full = repmat(activity_vec_init,1,3);

% binding info vecs
n_right_bound = repmat(n_right_bound_init,1,3);
n_right_bound(baseNum+1:2*baseNum) = n_right_bound(baseNum+1:2*baseNum) + 1; % second 6 state plane has 1cr bound to non-specific site

n_wrong_bound = repmat(n_wrong_bound_init,1,3);
n_wrong_bound(2*baseNum+1:3*baseNum) = n_wrong_bound(2*baseNum+1:3*baseNum) + 1; % third 6 state plane has 1cw bound to non-specific site

n_total_bound = n_wrong_bound + n_right_bound;

% add cross-plane connections. Let us assume the 6 state plane that is
% equivalent to the simpler 6 state model considered correspond to the
% first block
for i = 1:baseNum
    binding_ids = [1 2];
    for j = 1:2
        ind1 = i;
        ind2 = baseNum*j + i;
        
        % add to array
        permittedConnections(ind1,ind2) = 1;
        permittedConnections(ind2,ind1) = 1;
        
        % add binding info
        transitionInfoArray(ind2,ind1) = binding_ids(j); % specify whether it is a cognate or non-cognate factor binding
        transitionInfoArray(ind1,ind2) = -2; % all unbinding events from non-specific site are equivalent
    end
end
permittedConnections = permittedConnections==1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate symbolic transition rate matrix

% initialize core rate multiplier variables
syms cr cw b positive

% initialize core locus activity transition rates
syms kip kap kim kam positive

% initialize core binding rates
syms kpi kpa kmi kma positive

% initialize weights to allow for impact of 2 bound
syms wip wap wma wmi positive

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% specify var order, var bounds, and whether it can be swept by default
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
networkInfo = struct;
networkInfo.sweepVarList = [cr cw b kip kap kim kam kpi kpa kmi kma wip wap wma wmi];
networkInfo.defaultValues = [1 100 100 1 1 1 1 1 1 1 1 1 1 1 1];
networkInfo.wrongCycleFlags = [0 -1 1 1 1 1 1 1 1 1 1 0 0 0 0];
networkInfo.sweepFlags = [0 0 0 1 1 1 1 1 1 1 1 1 1 1 1]==1;
networkInfo.bindingFlags = [0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 ];
networkInfo.unbindingFlags = [0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ];
networkInfo.paramBounds = repmat([-4 ; 4 ], 1, length(networkInfo.sweepFlags));
networkInfo.paramBounds(:,1:3) = 1;
networkInfo.paramBounds(1,end-3:end) = 1;
networkInfo.cr_index = 1;
networkInfo.cw_index = 2;
networkInfo.b_index = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% perform basic assignments based on type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RSym = sym(zeros(nStates));

% basic binding and unbinding rates
RSym(ismember(transitionInfoArray,[1,2]) & ~activity_vec_full) = kpi;
RSym(ismember(transitionInfoArray,[1,2]) & activity_vec_full) = kpa;

RSym(ismember(transitionInfoArray,-[1,2]) & ~activity_vec_full) = kmi;
RSym(ismember(transitionInfoArray,-[1,2]) & activity_vec_full) = kma;

% basic locus fluctuation rates
RSym(ismember(transitionInfoArray,3) & n_total_bound==0) = kam;
RSym(ismember(transitionInfoArray,3) & n_total_bound==1) = kap;
RSym(ismember(transitionInfoArray,3) & n_total_bound>1) = wap*kap;

RSym(ismember(transitionInfoArray,-3) & n_total_bound==0) = kim;
RSym(ismember(transitionInfoArray,-3) & n_total_bound==1) = kip;
RSym(ismember(transitionInfoArray,-3) & n_total_bound>1) = wip*kip;

% layer on 2-bound multipliers for unbinding 
f1 = ismember(transitionInfoArray,-[1,2]) & n_total_bound==2 & ~activity_vec_full;
RSym(f1) = RSym(f1) / wmi;

f2 = ismember(transitionInfoArray,-[1,2]) & n_total_bound==2 & activity_vec_full;
RSym(f2) = RSym(f2) / wma;

% add specificity and concentration factors
RSym(transitionInfoArray==1) = RSym(transitionInfoArray==1)*cr;
RSym(transitionInfoArray==2) = RSym(transitionInfoArray==2)*cw;
RSym(transitionInfoArray==-2) = RSym(transitionInfoArray==-2)*b;

% add diagonal factors 
RSym(eye(size(RSym,1))==1) = -sum(RSym);

% wrte to file
RSymFun = matlabFunction(RSym,'File',[savePath 'RSymFun'],'Optimize',true,...
          'Vars',networkInfo.sweepVarList);       
        
% save helper variables
activeStates = find(activity_vec_full);
networkInfo.nStates = nStates;
networkInfo.activeStates = activeStates;
networkInfo.permittedConnections = permittedConnections;
networkInfo.transitionInfoArray = transitionInfoArray;
networkInfo.n_right_bound = n_right_bound;
networkInfo.n_wrong_bound = n_wrong_bound;
networkInfo.n_total_bound = n_total_bound;
networkInfo.activeStateFilter = activity_vec_full;

save([savePath 'networkInfo'],'networkInfo');        

%% Experiment with rapid HM calculations
% lets see if we can use anonymous functions for this
% RTest = RSym;
options = optimoptions(@lsqnonlin,'Display','off','MaxIterations',5);
optionsMin = optimoptions(@fmincon,'Display','off');
n_trials = 1e3;
new_r_array = NaN(size(RSym,1),size(RSym,2),n_trials);
tic;
for i = 1:n_trials
    % initialize random vector of values
    val = rand(1,15);
    valCell = mat2cell(val,size(val,1),ones(1,size(val,2)));

    % RAlt = subHelper(valVec,a,10)
    subIndices = [5 7];
    add_factor_fun = @(a) subHelper(val,a,subIndices);

    %%define objective function
    ob_fun = @(a) hmCalcHelper(add_factor_fun(a),activity_vec_full==1);    
    a_fit = lsqnonlin(ob_fun,1,0,1e4,options);

    new_r_array(:,:,i) = subHelper(val,a_fit,subIndices);
end

toc
ss_array = NaN(n_trials,size(RSym,2));
for i = 1:n_trials
    ss_array(i,:) = calculate_ss_num(new_r_array(:,:,i),10);
end

pd_rates = sum(ss_array(:,activity_vec_full==1),2);

%%
tic
r_cell = cell(1,n_trials);
for n = 1:n_trials
  r_cell{n} = new_r_array(:,:,n);
end
[V,D] = cellfun(@eig,r_cell,'UniformOutput',false);
D_cat = cat(3,D{:});
V_cat = cat(3,V{:});

% use linear indexing to extract eigenvalues
depth_index = reshape((0:n_trials-1)*numel(RSym),1,1,[]);
diag_indices = (0:size(RSym,1)-1)*size(RSym,1) + (1:size(RSym,1)) + depth_index;
D_diag = D_cat(diag_indices);
[mv,mi] = max(real(D_diag),[],2);

% now extract appropriate V vectors for each array
col_row_indices = size(RSym,1)*(mi-1) + (1:size(RSym,1))' + depth_index;
ss_array = V_cat(col_row_indices);
ss_array = ss_array ./ sum(ss_array);
toc

%%
tic
R_Array = NaN(size(new_r_array));
for i = 1:n_trials
    val = rand(1,15);
    valCell = mat2cell(val,size(val,1),ones(1,size(val,2)));
    R_Array(:,:,i) = RSymFun(valCell{:});
end
toc

%%
n_trials = 1e2;
tic
valMat = rand(n_trials,15);
nStates = 18;
% RAlt = subHelper(valVec,a,10)
subIndices = [4 5];
add_factor_fun = @(a) subHelperVec(valMat,a,subIndices,nStates);

options = optimoptions(@lsqnonlin,'Display','off','MaxIterations',15);

%%define objective function
ob_fun_vec = @(a) hmCalcHelperVec(add_factor_fun(a),activity_vec_full==1,nStates);

a_fit = fmincon(ob_fun_vec,ones(n_trials,1),[],[],[],[],zeros(n_trials,1),1e4*ones(n_trials,1),[],optionsMin);
toc
% new_r_array(:,:,i) = subHelper(val,a_fit,subIndices);






% R_cell = cellfun(@RSymFun,valCell,'UniformOutput',false); 
% RSymOld = RSym;
% RSym = RSymFun(valCell{:});
% add_factor_fun = @(a) mat2cell(val,size(val,1),ones(1,size(val,2)));



% ETONFun = matlabFunction(ETONMean,'File',[savePath 'TauOFFFunction'],'Optimize',true);
% 
% %%% mean time to go ON->OFF %%%
% onStateFilter = ismember(stateIndex,activeStates);
% 
% % create symbolic vector of passage times
% ETVecOFF = sym('ET%dOFF', [1 nStates]);
% ETVecOFF(~onStateFilter) = 0;
% 
% % create adjusted transition matrix
% RSymOFF = RSym;
% Rdiag = -reshape(diag(RSymOFF),1,[]);
% RSymOFF = RSymOFF ./ Rdiag;
% 
% RSymOFF(:,~onStateFilter) = 0;
% RSymOFF(eye(size(RSymOFF))==1) = 0;
% 
% % generate system of equations and solve
% eqSysOFF = ETVecOFF * RSymOFF + 1./Rdiag;
% eqSysOFF = eqSysOFF(onStateFilter);
% eqSysOFF = eqSysOFF == ETVecOFF(onStateFilter);
% eqSolOFF = solve(eqSysOFF,ETVecOFF(onStateFilter));
% 
% % transform results into vector and calculate the weighted avers
% solVecOFF = struct2array(eqSolOFF);
% % calculate incoming flux from OFF to ON states
% inFluxVecON = RSym(onStateFilter,~onStateFilter) * ssVecSym(~onStateFilter)';
% ETOFFMean = (solVecOFF*inFluxVecON) / sum(inFluxVecON);
% ETOFFFun = matlabFunction(ETOFFMean,'File',[savePath 'TauONFunction'],'Optimize',true);
% 
% %%% Cycle Time %%%
% TauCycleFun = matlabFunction(ETONMean+ETOFFMean,'File',[savePath 'TauCycleFunction'],'Optimize',true);
% 
% %%% %%%%%%%%%%%%%%%%%%%% Energy Dissipation Expressions %%%%%%%%%%%%%%%%%%%%
% 
% %%% Entropy production %%%
% % See equation 5 in: Thermodynamics of Statistical Inference by Cells
% 
% entropyRateSym = 0;
% 
% for i = 1:nStates
%     for j = 1:nStates
%         if i ~= j && RSym(i,j)~=0
%             entropyRateSym = entropyRateSym + ssVecSym(i)*RSym(j,i)*log(RSym(j,i)/RSym(i,j));
%         end
%     end
% end
% 
% matlabFunction(entropyRateSym,'File',[savePath 'entropyRateFunction'],'Optimize',true);
% 
% %%% calculate flux (only well-defined for single-loop systems)
% % netFluxSym = ssVecSym(1)*RSym(4,1) - ssVecSym(4)*RSym(1,4);
% % netFluxFun = matlabFunction(netFluxSym);
% % matlabFunction(netFluxSym,'File',[savePath 'netFluxFunction4State']);
% 
% % %%% forward and backward fluxes
% % [Numerator, Denominator] = numden(productionRateSym);
% % forwardFluxSym = RSym(1,2)*RSym(2,3)*RSym(3,4)*RSym(4,1) / Denominator;
% % forwardFluxFun = matlabFunction(forwardFluxSym);
% % matlabFunction(forwardFluxSym,'File',[savePath 'forwardFluxFunction4State']);
% % 
% % backwardFluxSym = RSym(2,1)*RSym(3,2)*RSym(4,3)*RSym(1,4) / Denominator;
% % backwardFluxFun = matlabFunction(backwardFluxSym);
% % matlabFunction(backwardFluxSym,'File',[savePath 'backwardFluxFunction4State']);
% 
