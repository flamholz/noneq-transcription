% This script generates helper functions for numeric metric calculations
clear
close all

% generate path to save metric functions 
savePath = '../utilities/metricFunctions/n6_OR_NUM/';
mkdir(savePath);
addpath(savePath)

% define some basic parameters
activeStates = [3 4 5];
baseNum = 6;
nStates = 6;

% helper vector
stateIndex = 1:nStates;

% zero out forbidden transitions
[fromRef,toRef] = meshgrid(1:baseNum,1:baseNum);
diffRef = abs(fromRef-toRef);
toRefHalved = toRef<=baseNum/2;
fromRefHalved = fromRef<=baseNum/2;
permittedConnectionsRaw= (diffRef==1 & toRefHalved==fromRefHalved) | diffRef==baseNum/2;

% permute these connections to follow a more intuitive labeling scheme
indexCurr = 1:baseNum;
indexAdjusted = circshift(indexCurr,floor(baseNum/4));
indexAdjusted = [indexAdjusted(1:baseNum/2) fliplr(indexAdjusted(baseNum/2+1:end))];
[~,si] = sort(indexAdjusted);
permittedConnections = permittedConnectionsRaw(si,si);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate an array with binding info
transitionInfoArray = zeros(size(permittedConnections));

% specific binding/unbinding
spec_pairs = {[1,2],[4,3]};
for s = 1:length(spec_pairs)
    ind1 = spec_pairs{s}(1);
    ind2 = spec_pairs{s}(2);
    % update
    transitionInfoArray(ind2,ind1) = 1;
    transitionInfoArray(ind1,ind2) = -1;
end

% non-specific binding/unbinding
non_spec_pairs = {[1,6],[4,5]};
for s = 1:length(non_spec_pairs)
    ind1 = non_spec_pairs{s}(1);
    ind2 = non_spec_pairs{s}(2);
    % update
    transitionInfoArray(ind2,ind1) = 2;
    transitionInfoArray(ind1,ind2) = -2;
end

% locus activity fluctuations
locus_pairs = {[1,4],[6,5],[2,3]};
for s = 1:length(locus_pairs)
    ind1 = locus_pairs{s}(1);
    ind2 = locus_pairs{s}(2);
    % update
    transitionInfoArray(ind2,ind1) = 3;
    transitionInfoArray(ind1,ind2) = -3;
end

% generate array tht indicates activity state
activity_vec_full = false(1,baseNum);
activity_vec_full(activeStates) = 1;

% generate flags indicating numbers of right and wrong factors bound
n_right_bound = zeros(size(activity_vec_full));
n_wrong_bound = n_right_bound;
n_right_bound([2 3]) = 1;
n_wrong_bound([5 6]) = 1;

permittedConnections = permittedConnections==1;
n_total_bound = n_wrong_bound + n_right_bound;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate symbolic transition rate matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initialize core rate multiplier variables
syms cr cw b positive

% initialize core locus activity transition rates
syms kip kap kim kam positive

% initialize core binding rates
syms kpi kpa kmi kma positive

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% specify var order, var bounds, and whether it can be swept by default
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
networkInfo = struct;
networkInfo.sweepVarList = [cr cw b kip kap kim kam kpi kpa kmi kma];
networkInfo.defaultValues = [1 100 100 1 1 1 1 1 1 1 1];
networkInfo.sweepFlags = [0 0 0 1 1 1 1 1 1 1 1]==1;
networkInfo.bindingFlags = [0 0 0 0 0 0 0 1 1 0 0];
networkInfo.unbindingFlags = [0 0 0 0 0 0 0 0 0 1 1];
networkInfo.paramBounds = repmat([-4 ; 4 ], 1, length(networkInfo.sweepFlags));
networkInfo.paramBounds(:,1:3) = 1;
networkInfo.cr_index = 1;
networkInfo.cw_index = 2;
networkInfo.b_index = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% perform basic assignments based on type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RSym = sym(zeros(nStates));

% basic binding and unbinding rates
RSym(ismember(transitionInfoArray,[1,2]) & ~activity_vec_full) = kpi;
RSym(ismember(transitionInfoArray,[1,2]) & activity_vec_full) = kpa;

RSym(ismember(transitionInfoArray,-[1,2]) & ~activity_vec_full) = kmi;
RSym(ismember(transitionInfoArray,-[1,2]) & activity_vec_full) = kma;

% basic locus fluctuation rates
RSym(ismember(transitionInfoArray,3) & n_total_bound==0) = kam;
RSym(ismember(transitionInfoArray,3) & n_total_bound==1) = kap;

RSym(ismember(transitionInfoArray,-3) & n_total_bound==0) = kim;
RSym(ismember(transitionInfoArray,-3) & n_total_bound==1) = kip;

% add specificity and concentration factors
RSym(transitionInfoArray==1) = RSym(transitionInfoArray==1)*cr;
RSym(transitionInfoArray==2) = RSym(transitionInfoArray==2)*cw;
RSym(transitionInfoArray==-2) = RSym(transitionInfoArray==-2)*b;

% add diagonal factors 
RSym(eye(size(RSym,1))==1) = -sum(RSym);

% save
RSymFun = matlabFunction(RSym,'File',[savePath 'RSymFun'],'Optimize',true,...
          'Vars',networkInfo.sweepVarList);   

% save helper variables
networkInfo.nStates = nStates;
networkInfo.activeStates = activeStates;
networkInfo.permittedConnections = permittedConnections;
networkInfo.transitionInfoArray = transitionInfoArray;
networkInfo.n_right_bound = n_right_bound;
networkInfo.n_wrong_bound = n_wrong_bound;
networkInfo.n_total_bound = n_total_bound;
networkInfo.activeStateFilter = activity_vec_full;

save([savePath 'networkInfo'],'networkInfo');
%% get list of active states
activeStates = find(activity_vec_full);
ssVecSym = sym('ss%d', [1 nStates],'positive');

%%% attempt to derive formula for flux constraint
% initialize flux variable
syms J real

% develop matrix that encodes directionality of each transition
fluxSignArray = 1*permittedConnections;

% designate transitions that are negative by our convention
% basic binding and unbinding rates
fluxSignArray(ismember(transitionInfoArray,[1,2]) & ~activity_vec_full) = 1; % this is redundant but I'll do it for the sake of clarity
fluxSignArray(ismember(transitionInfoArray,[1,2]) & activity_vec_full) = -1; 

fluxSignArray(ismember(transitionInfoArray,-[1,2]) & ~activity_vec_full) = -1;
fluxSignArray(ismember(transitionInfoArray,-[1,2]) & activity_vec_full) = 1;

% basic locus fluctuation rates
fluxSignArray(ismember(transitionInfoArray,3) & n_total_bound==0) = -2; % central transitions have double the flux
fluxSignArray(ismember(transitionInfoArray,3) & n_total_bound==1) = 1;

fluxSignArray(ismember(transitionInfoArray,-3) & n_total_bound==0) = 2;
fluxSignArray(ismember(transitionInfoArray,-3) & n_total_bound==1) = -1;

% generate signed symbolic flux array
J_mat = fluxSignArray*J;

% construct system of equations
out_flux_mat = RSym .* ssVecSym;
eq_vec = sym(zeros(1,7));
iter = 1;
for i = 1:size(RSym,2)
    for j = i+1:size(RSym,1)      
        if RSym(j,i) ~= 0          
            eq_vec(iter) = out_flux_mat(j,i)-out_flux_mat(i,j) == J_mat(j,i);
            iter = iter + 1;
        end
    end
end
            
      
% solve
sol = solve(eq_vec([1 2 4 5]),[kpi kpa kam kap],'ReturnConditions',false);

%%
etSolVec = struct2array(sol);
etSolFun = matlabFunction(etSolVec);
in_ids = [NaN 2 6 7 8 9 NaN NaN NaN];
out_ids = [11 10 4 5];
cw = 1.5;
b = 123;

% test
ss_vec_in = rand(1,6);
ss_vec_in = ss_vec_in/sum(ss_vec_in);
J = 100;
rates = rand(1,5);

val_inputs = [J rates ss_vec_in(1:4)];
valCell = mat2cell(val_inputs,size(val_inputs,1),ones(1,size(val_inputs,2)));
%    RSYM = RSYMFUN(B,CR,CW,KAM,KAP,KIM,KIP,KMA,KMI,KPA,KPI)
sol_out = etSolFun(valCell{:})



% tic;
% ss_num_array = [];
% for i = 1:1e3
%     val = rand(1,11);
%     valCell = mat2cell(val,size(val,1),ones(1,size(val,2)));
%     activeStateFilter = ismember(1:nStates,activeStates);
% 
%     Q_num = RSymFun(valCell{:});
%     ss_num = calculate_ss_num(Q_num);
% %     ss_num_array(i,:) = ss_num;
%     Z_num = calculate_Z_matrix(Q_num,ss_num);
%     var_num = calculate_var_num(Q_num,ss_num,activeStateFilter,Z_num);
%     phi_num = calculate_entropy_rate_num(Q_num,ss_num);
% 
%     [Tau_ON_num,Tau_OFF_num,cycle_time] = calculate_tau_num(Q_num,ss_num,activeStateFilter);
% end
% toc
% % quick check
% % r1 = Tau_ON_num / (Tau_ON_num + Tau_OFF_num)
% % r2 = sum(ss_num(activeStateFilter))
%%


% production rate
% productionRateSym = sum(ssVecSym(activeStates));
% productionRateFun = matlabFunction(productionRateSym,'File',[savePath 'productionRateFunction'],'Optimize',true);
% 
% % take derivative wrpt c to get sharpness
% % sharpnessSym = diff(productionRateSym,c);
% % sharpnessFun = matlabFunction(sharpnessSym,'File',[savePath 'sharpnessFunction'],'Optimize',true);
% 
% % take derivative wrpt c for correct state only
% % sharpnessRightSym = diff(ssVecSym(activeStates(1:2)),c);
% % matlabFunction(sharpnessRightSym,'File',[savePath 'sharpnessRightFunction'],'Optimize',true);
% 
% 
% %% %%%%%%%%%%%%%%%%%%%%%%%% Half max constraints %%%%%%%%%%%%%%%%%%%%%%%%%%
% % to start, we've gotta make a simplified version of the full 6 state model
% % that assumes symmetry between right and wrong cycles
% 
% % for i = 4
% %     tic
% %     nFun = 3;
% %     if i == 1
% %         nFun = 2;
% %     end
% %     for j = 1:nFun
% %         numStr = [num2str(i) num2str(j)];
% %         try        
% %             eval(['a' numStr 'Sym = a' numStr 'FunFromMathematica(RSymRaw);'])
% %             savePathFull = [savePath 'a' numStr 'SymFun'];
% %             eval(['matlabFunction(a' numStr 'Sym,"File",savePathFull,"Optimize",true);'])
% %         catch
% %             disp(['error in ' numStr])
% %         end
% %     end
% %     toc
% % end
% 
% %%
% % generate and solve systems of equations for expected first passage time
% % to each active state
% 
% %%
% val = rand(1,13);
% valCell = mat2cell(val,size(val,1),ones(1,size(val,2)));
% RSym = RSymFun(valCell{:})
% etInfo = struct;
% tic
% for CurrentState = 1:nStates
%     
%     % create symbolic vector of passage times
%     etInfo(CurrentState).ETVec = sym(['ET%d' num2str(CurrentState)], [1 nStates]);
%     etInfo(CurrentState).ETVec(CurrentState) = 0;
%     
%     % create adjusted transition matrix
%     etInfo(CurrentState).RSymTo = RSym;
%     etInfo(CurrentState).Rdiag = -reshape(diag(etInfo(CurrentState).RSymTo),1,[]);
%     etInfo(CurrentState).RSymTo = etInfo(CurrentState).RSymTo ./ etInfo(CurrentState).Rdiag;
%     etInfo(CurrentState).RSymTo(:,CurrentState) = 0;
%     etInfo(CurrentState).RSymTo(eye(size(etInfo(CurrentState).RSymTo))==1) = 0;
%     
%     % create system of equations
%     etInfo(CurrentState).eqSys = etInfo(CurrentState).ETVec * etInfo(CurrentState).RSymTo + 1./etInfo(CurrentState).Rdiag;
%     
%     stateFilter = stateIndex~=CurrentState;    
%     
%     etInfo(CurrentState).eqSys = etInfo(CurrentState).eqSys(stateFilter);
%     etInfo(CurrentState).eqSys = etInfo(CurrentState).eqSys == etInfo(CurrentState).ETVec(stateFilter);
%     
%     % solve
%     etInfo(CurrentState).eqSol = solve(etInfo(CurrentState).eqSys,etInfo(CurrentState).ETVec(stateFilter));
%     etInfo(CurrentState).etSolVec = struct2array(etInfo(CurrentState).eqSol);  
%     etInfo(CurrentState).etSolVec = [etInfo(CurrentState).etSolVec(1:CurrentState-1) 0 etInfo(CurrentState).etSolVec(CurrentState:end)];
%     etInfo(CurrentState).ETMean = etInfo(CurrentState).etSolVec*ssVecSym';
%         
% end
% toc
% %% %%%%%%%%%%%%%%%%%%% get expressions for variance %%%%%%%%%%%%%%%%%%%%%%%
% 
% f_vec = zeros(1,nStates); % initiation rate for each state
% f_vec(activeStates) = 1; % assume all active states produce at the same rate
% 
% % construct Z matrix 
% % see eqs 28 and 29 from: "Asymptotic Formulas for Markov Processes with
% %                          Applications to Simulation"
% ZSym = sym(size(RSym));
% for i = 1:nStates
%     for j = 1:nStates
%         if i == j
%             ZSym(i,j) = ssVecSym(j)*etInfo(j).ETMean;
%         else
%             ZSym(i,j) = ssVecSym(j)*(etInfo(j).ETMean-etInfo(j).etSolVec(i));
%         end
%     end
% end
% 
% % now caculate the variance (see eq 12 from above source)
% varSum = 0;
% for i = 1:size(RSym,2)
%     for j = 1:size(RSym,2)
%         varSum = varSum + f_vec(i)*ssVecSym(i)*ZSym(i,j)*f_vec(j);
%     end
% end
% varSum = 2*varSum;
% 
% % convert to function
% tic
% VarFun = matlabFunction(varSum,'File',[savePath 'intrinsicVarianceFunction'],'Optimize',true);
% toc


% %% %%%%%%%%%%%%%%%%%%%%%%%%% Cycle time calculations %%%%%%%%%%%%%%%%%%%%%%%
% % 
% % %%% mean time to go OFF->ON %%%
% val = rand(1,13);
% valCell = mat2cell(val,size(val,1),ones(1,size(val,2)));
% RSymOld = RSym;
% RSym = RSymFun(valCell{:});
% 
% 
% % Experiment with making a condensed transition matrix
% 
% % truncate matrix
% tic
% for i = 1:100
%     RNumTrunc = zeros(nStates/2+1,nStates/2+1);
%     RNumTrunc(1:end-1,1:end-1) = RSym(offStateFilter,offStateFilter);
%     RNumTrunc(end,1:end-1) = sum(RSym(~offStateFilter,offStateFilter));
%     RNumTrunc(1:end-1,end) = sum(RSym(offStateFilter,~offStateFilter),2);
%     RNumTrunc(end,end) = -sum(RNumTrunc(:,end));
% 
%     % calculate SS
%     [V,D] = eig(RNumTrunc);
%     [~,mi] = max(real(diag(D)));
%     ss_vec_trunc = V(:,mi)./sum(V(:,mi));
% 
%     % calculate Z
%     PI_trunc = repmat(ss_vec_trunc',size(RNumTrunc,1),1);
%     Z_trunc = inv(PI_trunc-RNumTrunc') - PI_trunc;
% 
%     % calculate ON times
%     ssON = ss_vec_trunc(end);
%     ETON_vec = (Z_trunc(end,end) - Z_trunc(1:end-1,end))/ssON;
%     
%     % calculate flux-weighted mean
%     inFluxVecOFF = RSym(offStateFilter,~offStateFilter) * ss_vec_full(~offStateFilter);
%     ETON_Mean = (inFluxVecOFF'*ETON_vec) / sum(inFluxVecOFF);
% end
% toc
% % ETON_mean = ss_vec(1:end-1)' * ETON_vec
% %% Experiment with rapid HM calculations
% % lets see if we can use anonymous functions for this
% % RTest = RSym;
% options = optimoptions(@lsqnonlin,'Display','off');
% optionsMin = optimoptions(@fmincon,'Display','off');
% 
% tic;
% for i = 1:1
%     val = rand(1,13);
%     valCell = mat2cell(val,size(val,1),ones(1,size(val,2)));
% 
%     % RAlt = subHelper(valVec,a,10)
%     subIndices = [4 5];
% 
% 
%     add_factor_fun = @(a) subHelper(val,a,subIndices);
% 
%     % test1 = add_factor_fun(1e5);
%     % test2 = add_factor_fun(1e0);
%     % heh = test1./test2;
%     % heh(eye(size(heh))==1) = 0;
%     %%define objective function
% 
%     ob_fun = @(a) hmCalcHelper(add_factor_fun(a),~offStateFilter);
%     % a_fit = fmincon(ob_fun,1,[],[],[],[],0,1e4,[],optionsMin);
%     a_fit = lsqnonlin(ob_fun,1,0,1e4,options);
% 
% end
% % a_fit = fmincon(ob_fun,1,[],[],[],[],0,1e4);
% toc
% % RSymOld = RSym;
% % RSym = RSymFun(valCell{:});
% % add_factor_fun = @(a) mat2cell(val,size(val,1),ones(1,size(val,2)));
% 
% 
% 
% % ETONFun = matlabFunction(ETONMean,'File',[savePath 'TauOFFFunction'],'Optimize',true);
% % 
% % %%% mean time to go ON->OFF %%%
% % onStateFilter = ismember(stateIndex,activeStates);
% % 
% % % create symbolic vector of passage times
% % ETVecOFF = sym('ET%dOFF', [1 nStates]);
% % ETVecOFF(~onStateFilter) = 0;
% % 
% % % create adjusted transition matrix
% % RSymOFF = RSym;
% % Rdiag = -reshape(diag(RSymOFF),1,[]);
% % RSymOFF = RSymOFF ./ Rdiag;
% % 
% % RSymOFF(:,~onStateFilter) = 0;
% % RSymOFF(eye(size(RSymOFF))==1) = 0;
% % 
% % % generate system of equations and solve
% % eqSysOFF = ETVecOFF * RSymOFF + 1./Rdiag;
% % eqSysOFF = eqSysOFF(onStateFilter);
% % eqSysOFF = eqSysOFF == ETVecOFF(onStateFilter);
% % eqSolOFF = solve(eqSysOFF,ETVecOFF(onStateFilter));
% % 
% % % transform results into vector and calculate the weighted avers
% % solVecOFF = struct2array(eqSolOFF);
% % % calculate incoming flux from OFF to ON states
% % inFluxVecON = RSym(onStateFilter,~onStateFilter) * ssVecSym(~onStateFilter)';
% % ETOFFMean = (solVecOFF*inFluxVecON) / sum(inFluxVecON);
% % ETOFFFun = matlabFunction(ETOFFMean,'File',[savePath 'TauONFunction'],'Optimize',true);
% % 
% % %%% Cycle Time %%%
% % TauCycleFun = matlabFunction(ETONMean+ETOFFMean,'File',[savePath 'TauCycleFunction'],'Optimize',true);
% % 
% % %%% %%%%%%%%%%%%%%%%%%%% Energy Dissipation Expressions %%%%%%%%%%%%%%%%%%%%
% % 
% % %%% Entropy production %%%
% % % See equation 5 in: Thermodynamics of Statistical Inference by Cells
% % 
% % entropyRateSym = 0;
% % 
% % for i = 1:nStates
% %     for j = 1:nStates
% %         if i ~= j && RSym(i,j)~=0
% %             entropyRateSym = entropyRateSym + ssVecSym(i)*RSym(j,i)*log(RSym(j,i)/RSym(i,j));
% %         end
% %     end
% % end
% % 
% % matlabFunction(entropyRateSym,'File',[savePath 'entropyRateFunction'],'Optimize',true);
% % 
% % %%% calculate flux (only well-defined for single-loop systems)
% % % netFluxSym = ssVecSym(1)*RSym(4,1) - ssVecSym(4)*RSym(1,4);
% % % netFluxFun = matlabFunction(netFluxSym);
% % % matlabFunction(netFluxSym,'File',[savePath 'netFluxFunction4State']);
% % 
% % % %%% forward and backward fluxes
% % % [Numerator, Denominator] = numden(productionRateSym);
% % % forwardFluxSym = RSym(1,2)*RSym(2,3)*RSym(3,4)*RSym(4,1) / Denominator;
% % % forwardFluxFun = matlabFunction(forwardFluxSym);
% % % matlabFunction(forwardFluxSym,'File',[savePath 'forwardFluxFunction4State']);
% % % 
% % % backwardFluxSym = RSym(2,1)*RSym(3,2)*RSym(4,3)*RSym(1,4) / Denominator;
% % % backwardFluxFun = matlabFunction(backwardFluxSym);
% % % matlabFunction(backwardFluxSym,'File',[savePath 'backwardFluxFunction4State']);
% % 
