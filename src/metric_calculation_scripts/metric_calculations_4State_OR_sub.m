% This script generate symbolic expressions for 4 state network that are
% compatibile with format used for 6 and 18 state numeric scripts
clear
close all
saveRoot = '../utilities/metricFunctions/';
savePathCell = {[saveRoot 'n18_OR_NUM/n4_symbolic_functions' filesep],[saveRoot 'n6_OR_NUM/n4_symbolic_functions' filesep]};

for s = 1:length(savePathCell)                
    mkdir(savePathCell{s});
    addpath(savePathCell{s})
end
% define some basic parameters
activeStatesBase = [3 4 5];
baseNum = 6;
nStates = 18;
% sensingEdges = [2,1 ; 3,4];
% stateIndex = 1:nStates;

% define symbolic variables
RSymFull = sym('k%d%d', [nStates nStates],'positive');

% zero out forbidden transitions
[fromRef,toRef] = meshgrid(1:baseNum,1:baseNum);
diffRef = abs(fromRef-toRef);
toRefHalved = toRef<=baseNum/2;
fromRefHalved = fromRef<=baseNum/2;
permittedConnectionsRaw= (diffRef==1 & toRefHalved==fromRefHalved) | diffRef==baseNum/2;

% permute these connections to follow a more intuitive labeling scheme
indexCurr = 1:baseNum;
indexAdjusted = circshift(indexCurr,floor(baseNum/4));
indexAdjusted = [indexAdjusted(1:baseNum/2) fliplr(indexAdjusted(baseNum/2+1:end))];
[~,si] = sort(indexAdjusted);
permittedConnectionsInit = permittedConnectionsRaw(si,si);

% generate an array with binding info
transitionInfoInit = zeros(size(permittedConnectionsInit));

% specific binding/unbinding
spec_pairs = {[1,2],[4,3]};
for s = 1:length(spec_pairs)
    ind1 = spec_pairs{s}(1);
    ind2 = spec_pairs{s}(2);
    % update
    transitionInfoInit(ind2,ind1) = 1;
    transitionInfoInit(ind1,ind2) = -1;
end

% non-specific binding/unbinding
non_spec_pairs = {[1,6],[4,5]};
for s = 1:length(non_spec_pairs)
    ind1 = non_spec_pairs{s}(1);
    ind2 = non_spec_pairs{s}(2);
    % update
    transitionInfoInit(ind2,ind1) = 2;
    transitionInfoInit(ind1,ind2) = -2;
end

% locus activity fluctuations
locus_pairs = {[1,4],[6,5],[2,3]};
for s = 1:length(locus_pairs)
    ind1 = locus_pairs{s}(1);
    ind2 = locus_pairs{s}(2);
    % update
    transitionInfoInit(ind2,ind1) = 3;
    transitionInfoInit(ind1,ind2) = -3;
end

% generate array tht indicates activity state
activity_vec_init = false(1,baseNum);
activity_vec_init(activeStatesBase) = 1;

% generate flags indicating numbers of right and wrong factors bound
n_right_bound_init = zeros(size(activity_vec_init));
n_wrong_bound_init = n_right_bound_init;
n_right_bound_init([2 3]) = 1;
n_wrong_bound_init([5 6]) = 1;

% generate full 18 state array
rate_mask = repelem(eye(3),baseNum,baseNum);

% full connection array
permittedConnections = repmat(permittedConnectionsInit,3,3);%permittedConnectionsInit
permittedConnections = permittedConnections.*rate_mask;

% full binding array
transitionInfoArray = repmat(transitionInfoInit,3,3);%permittedConnectionsInit
transitionInfoArray = transitionInfoArray.*rate_mask;

% full activity vector
activity_vec_full = repmat(activity_vec_init,1,3);

% binding info vecs
n_right_bound = repmat(n_right_bound_init,1,3);
n_right_bound(baseNum+1:2*baseNum) = n_right_bound(baseNum+1:2*baseNum) + 1; % second 6 state plane has 1cr bound to non-specific site

n_wrong_bound = repmat(n_wrong_bound_init,1,3);
n_wrong_bound(2*baseNum+1:3*baseNum) = n_wrong_bound(2*baseNum+1:3*baseNum) + 1; % third 6 state plane has 1cw bound to non-specific site

n_total_bound = n_wrong_bound + n_right_bound;

% add cross-plane connections. Let us assume the 6 state plane that is
% equivalent to the simpler 6 state model considered correspond to the
% first block
for i = 1:baseNum
    binding_ids = [1 2];
    for j = 1:2
        ind1 = i;
        ind2 = baseNum*j + i;
        
        % add to array
        permittedConnections(ind1,ind2) = 1;
        permittedConnections(ind2,ind1) = 1;
        
        % add binding info
        transitionInfoArray(ind2,ind1) = binding_ids(j); % specify whether it is a cognate or non-cognate factor binding
        transitionInfoArray(ind1,ind2) = -2; % all unbinding events from non-specific site are equivalent
    end
end
permittedConnections = permittedConnections==1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate symbolic transition rate matrix

% initialize core rate multiplier variables
syms cr cw b positive

% initialize core locus activity transition rates
syms kip kap kim kam positive

% initialize core binding rates
syms kpi kpa kmi kma positive

% initialize weights to allow for impact of 2 bound
syms wip wap wma wmi positive

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% specify var order, var bounds, and whether it can be swept by default
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
networkInfo = struct;
networkInfo.sweepVarList = [cr cw b kip kap kim kam kpi kpa kmi kma wip wap wma wmi];
networkInfo.defaultValues = [1 100 100 1 1 1 1 1 1 1 1 1 1 1 1];
networkInfo.wrongCycleFlags = [0 -1 1 1 1 1 1 1 1 1 1 0 0 0 0];
networkInfo.sweepFlags = [0 0 0 1 1 1 1 1 1 1 1 1 1 1 1]==1;
networkInfo.bindingFlags = [0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 ];
networkInfo.unbindingFlags = [0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 ];
networkInfo.paramBounds = repmat([-4 ; 4 ], 1, length(networkInfo.sweepFlags));
networkInfo.paramBounds(:,1:3) = 1;
networkInfo.paramBounds(1,end-3:end) = 1;
networkInfo.cr_index = 1;
networkInfo.cw_index = 2;
networkInfo.b_index = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% perform basic assignments based on type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RSym = sym(zeros(nStates));

% basic binding and unbinding rates
RSym(ismember(transitionInfoArray,[1,2]) & ~activity_vec_full) = kpi;
RSym(ismember(transitionInfoArray,[1,2]) & activity_vec_full) = kpa;

RSym(ismember(transitionInfoArray,-[1,2]) & ~activity_vec_full) = kmi;
RSym(ismember(transitionInfoArray,-[1,2]) & activity_vec_full) = kma;

% basic locus fluctuation rates
RSym(ismember(transitionInfoArray,3) & n_total_bound==0) = kam;
RSym(ismember(transitionInfoArray,3) & n_total_bound==1) = kap;
RSym(ismember(transitionInfoArray,3) & n_total_bound>1) = wap*kap;

RSym(ismember(transitionInfoArray,-3) & n_total_bound==0) = kim;
RSym(ismember(transitionInfoArray,-3) & n_total_bound==1) = kip;
RSym(ismember(transitionInfoArray,-3) & n_total_bound>1) = wip*kip;

% layer on 2-bound multipliers for unbinding 
f1 = ismember(transitionInfoArray,-[1,2]) & n_total_bound==2 & ~activity_vec_full;
RSym(f1) = RSym(f1) / wmi;

f2 = ismember(transitionInfoArray,-[1,2]) & n_total_bound==2 & activity_vec_full;
RSym(f2) = RSym(f2) / wma;

% add specificity and concentration factors
RSym(transitionInfoArray==1) = RSym(transitionInfoArray==1)*cr;
RSym(transitionInfoArray==2) = RSym(transitionInfoArray==2)*cw;
RSym(transitionInfoArray==-2) = RSym(transitionInfoArray==-2)*b;

% add diagonal factors 
RSym(eye(size(RSym,1))==1) = -sum(RSym);

% wrte to file
% RSymFun = matlabFunction(RSym,'File',[savePathCell 'RSymFun'],'Optimize',true,...
%           'Vars',networkInfo.sweepVarList);       
        
% save helper variables
activeStates = find(activity_vec_full);
networkInfo.nStates = nStates;
networkInfo.activeStates = activeStates;
networkInfo.permittedConnections = permittedConnections;
networkInfo.transitionInfoArray = transitionInfoArray;
networkInfo.n_right_bound = n_right_bound;
networkInfo.n_wrong_bound = n_wrong_bound;
networkInfo.n_total_bound = n_total_bound;
networkInfo.activeStateFilter = activity_vec_full;

%% %%%%%%%%% derive expressions for production rate %%%%%%%%%%
sweepVarList18 = networkInfo.sweepVarList;
sweepVarList6 = networkInfo.sweepVarList(1:end-4);
sweepVarList4 = sweepVarList6([1 4:end]);
sweepVarCell = {sweepVarList18 sweepVarList6 sweepVarList4};
RSym4 = RSym(1:4,1:4);
RSym4(eye(size(RSym4,1))==1) = 0;
RSym4(eye(size(RSym4,1))==1) = -sum(RSym4);

[V,D] = eig(RSym4);
DLog = logical(D==0);
ssInd = find(all(DLog));
ssVecSym4 = V(:,ssInd) / sum(V(:,ssInd));
ssVecSym4 = ssVecSym4';

% full steady state vector 
for s = 1:length(savePathCell)
    ssVecFun = matlabFunction(ssVecSym4,'File',[savePathCell{s} 'n4steadyStateFunction'],'Optimize',true,'Vars',sweepVarCell{s});
end        

productionRateSym = sum(ssVecSym4(activeStates(activeStates<=4)));
for s = 1:length(savePathCell)
    productionRateFun = matlabFunction(productionRateSym,'File',[savePathCell{s} 'n4productionRateFunction'],'Optimize',true,'Vars',sweepVarCell{s});
end

%% %%%%%%%%% do the same for the "wrong cycle" %%%%%%%%%%

RSym4W = RSym([1 6 5 4],[1 6 5 4]);
RSym4W(eye(size(RSym4W,1))==1) = 0;
RSym4W(eye(size(RSym4W,1))==1) = -sum(RSym4W);

[VW,DW] = eig(RSym4W);
DLogW = logical(DW==0);
ssIndW = find(all(DLogW));
ssVecSym4W = VW(:,ssIndW) / sum(VW(:,ssIndW));
ssVecSym4W = ssVecSym4W';

% full steady state vector 
for s = 1:length(savePathCell)
    ssVecFunW = matlabFunction(ssVecSym4W,'File',[savePathCell{s} 'n4WrongsteadyStateFunction'],'Optimize',true,'Vars',sweepVarCell{s});
end        

productionRateSymW = sum(ssVecSym4W(activeStates(activeStates<=4)));
for s = 1:length(savePathCell)
    productionRateFunW = matlabFunction(productionRateSymW,'File',[savePathCell{s} 'n4WrongproductionRateFunction'],'Optimize',true,'Vars',sweepVarCell{s});
end

%% variance

% generate and solve systems of equations for expected first passage time
% to each active state
RSym = RSym4;
nStates = 4;
stateIndex = 1:nStates;
ssVecSym = ssVecSym4;

etInfo = struct;
for CurrentState = 1:nStates
      
    % create symbolic vector of passage times
    etInfo(CurrentState).ETVec = sym(['ET%d' num2str(CurrentState)], [1 nStates]);
    etInfo(CurrentState).ETVec(CurrentState) = 0;
    
    % create adjusted transition matrix
    etInfo(CurrentState).RSymTo = RSym;
    etInfo(CurrentState).Rdiag = -reshape(diag(etInfo(CurrentState).RSymTo),1,[]);
    etInfo(CurrentState).RSymTo = etInfo(CurrentState).RSymTo ./ etInfo(CurrentState).Rdiag;
    etInfo(CurrentState).RSymTo(:,CurrentState) = 0;
    etInfo(CurrentState).RSymTo(eye(size(etInfo(CurrentState).RSymTo))==1) = 0;
    
    % create system of equations
    etInfo(CurrentState).eqSys = etInfo(CurrentState).ETVec * etInfo(CurrentState).RSymTo + 1./etInfo(CurrentState).Rdiag;
    
    stateFilter = stateIndex~=CurrentState;    
    
    etInfo(CurrentState).eqSys = etInfo(CurrentState).eqSys(stateFilter);
    etInfo(CurrentState).eqSys = etInfo(CurrentState).eqSys == etInfo(CurrentState).ETVec(stateFilter);
    
    % solve
    etInfo(CurrentState).eqSol = solve(etInfo(CurrentState).eqSys,etInfo(CurrentState).ETVec(stateFilter));
    etInfo(CurrentState).etSolVec = struct2array(etInfo(CurrentState).eqSol);  
    etInfo(CurrentState).etSolVec = [etInfo(CurrentState).etSolVec(1:CurrentState-1) 0 etInfo(CurrentState).etSolVec(CurrentState:end)];
    etInfo(CurrentState).ETMean = etInfo(CurrentState).etSolVec*ssVecSym';
end
 
% %%%%%%%%%%%%%%%%%%% get expressions for variance %%%%%%%%%%%%%%%%%%%%%%%


f_vec = zeros(1,nStates); % initiation rate for each state
f_vec(activeStates) = 1; % assume all active states produce at the same rate

% construct Z matrix 
% see eqs 28 and 29 from: "Asymptotic Formulas for Markov Processes with
%                          Applications to Simulation"
ZSym = sym(size(RSym));
for i = 1:nStates
    for j = 1:nStates
        if i == j
            ZSym(i,j) = ssVecSym(j)*etInfo(j).ETMean;
        else
            ZSym(i,j) = ssVecSym(j)*(etInfo(j).ETMean-etInfo(j).etSolVec(i));
        end
    end
end

% now caculate the variance (see eq 12 from above source)
varSum = 0;
for i = 1:size(RSym,2)
    for j = 1:size(RSym,2)
        varSum = varSum + f_vec(i)*ssVecSym(i)*ZSym(i,j)*f_vec(j);
    end
end
varSum = 2*varSum;

% convert to function
tic
for s = 1:length(savePathCell)
    VarFun = matlabFunction(varSum,'File',[savePathCell{s} 'n4IntrinsicVarianceFunction'],'Optimize',true,'Vars',sweepVarCell{s});
end
toc
%% %%%%%%%%%%%%%%%%%%%%%%%%% Cycle time calculations %%%%%%%%%%%%%%%%%%%%%%%

%%% mean time to go OFF->ON %%%
activeStates = [3 4];
% create symbolic vector of passage times
ETVecON = sym('ET%dON', [1 nStates]);
ETVecON(activeStates) = 0;

% create adjusted transition matrix
RSymON = RSym;
Rdiag = -reshape(diag(RSymON),1,[]);
RSymON = RSymON ./ Rdiag;
RSymON(:,activeStates) = 0;
RSymON(eye(size(RSymON))==1) = 0;

% generate system of equations and solve
eqSysON = ETVecON * RSymON + 1./Rdiag;
offStateFilter = ~ismember(stateIndex,activeStates);
eqSysON = eqSysON(offStateFilter);
eqSysON = eqSysON == ETVecON(offStateFilter);
eqSolON = solve(eqSysON,ETVecON(offStateFilter));

% transform results into vector and calculate the weighted avers
solVecON = struct2array(eqSolON);

% calculate inbound flux into each OFF state from the ON States
inFluxVecOFF = RSym(offStateFilter,~offStateFilter) * ssVecSym(~offStateFilter)';
ETONMean = (solVecON*inFluxVecOFF) / sum(inFluxVecOFF);
for s = 1:length(savePathCell)
    matlabFunction(ETONMean,'File',[savePathCell{s} 'n4TauOFFFunction'],'Optimize',true,'Vars',sweepVarCell{s})
end

%%% mean time to go ON->OFF %%%
onStateFilter = ismember(stateIndex,activeStates);

% create symbolic vector of passage times
ETVecOFF = sym('ET%dOFF', [1 nStates]);
ETVecOFF(~onStateFilter) = 0;

% create adjusted transition matrix
RSymOFF = RSym;
Rdiag = -reshape(diag(RSymOFF),1,[]);
RSymOFF = RSymOFF ./ Rdiag;

RSymOFF(:,~onStateFilter) = 0;
RSymOFF(eye(size(RSymOFF))==1) = 0;

% generate system of equations and solve
eqSysOFF = ETVecOFF * RSymOFF + 1./Rdiag;
eqSysOFF = eqSysOFF(onStateFilter);
eqSysOFF = eqSysOFF == ETVecOFF(onStateFilter);
eqSolOFF = solve(eqSysOFF,ETVecOFF(onStateFilter));

% transform results into vector and calculate the weighted avers
solVecOFF = struct2array(eqSolOFF);
% calculate incoming flux from OFF to ON states
inFluxVecON = RSym(onStateFilter,~onStateFilter) * ssVecSym(~onStateFilter)';
ETOFFMean = (solVecOFF*inFluxVecON) / sum(inFluxVecON);
for s = 1:length(savePathCell)
    matlabFunction(ETOFFMean,'File',[savePathCell{s} 'n4TauONFunction'],'Optimize',true,'Vars',sweepVarCell{s});
end

%%% Cycle Time %%%
for s = 1:length(savePathCell)
    matlabFunction(ETONMean+ETOFFMean,'File',[savePathCell{s} 'n4TauCycleFunction'],'Optimize',true,'Vars',sweepVarCell{s});
end

%%% %%%%%%%%%%%%%%%%%%%% Energy Dissipation Expressions %%%%%%%%%%%%%%%%%%%%

%%% Entropy production %%%
% See equation 5 in: Thermodynamics of Statistical Inference by Cells

% entropyRateSym = 0;
% 
% for i = 1:nStates
%     for j = 1:nStates
%         if i ~= j && RSym(i,j)~=0
%             entropyRateSym = entropyRateSym + ssVecSym(i)*RSym(j,i)*log(RSym(j,i)/RSym(i,j));
%         end
%     end
% end
% for s = 1:length(savePathCell)
%     matlabFunction(entropyRateSym,'File',[savePathCell{s} 'entropyRateFunction'],'Optimize',true,'Vars',sweepVarCell{s});
% end

% Transfer files to n6 Sym
status = copyfile([saveRoot 'n6_OR_NUM/n4_symbolic_functions' filesep],[saveRoot 'n6_OR_v2/n4_symbolic_functions' filesep]);