% script to call core parameter sweep function to examine tradeoffs between
% different network behaviors

clear 
close all
addpath(genpath('../utilities/'))


[~,metric_names] = calculateMetricsNumeric([]);
nStates = 6;
% make sure we're linked to the appropriate function subfolder
rmpath(genpath('../utilities/metricFunctions/'));
addpath(genpath(['../utilities/metricFunctions/n6_OR_NUM/']));

% define save path
OutPath = ['../../out/bivariate_parameter_sweeps_n6_numeric' filesep];
mkdir(OutPath);
                         

% get index of useful metrics
flux_index = find(strcmp(metric_names,'Flux'));
rate_index = find(strcmp(metric_names,'Production Rate'));
spec_index = find(strcmp(metric_names,'Specificity'));
spec_alt_index = find(strcmp(metric_names,'specFactorAlt'));
dev_factor_index = find(strcmp(metric_names,'deviationFactor'));
sharp_right_index = find(strcmp(metric_names,'SharpnessRight'));
sharp_right_norm_index = find(strcmp(metric_names,'SharpnessRightNorm'));
sharpness_index = find(strcmp(metric_names,'Sharpness'));
sharpness_norm_index = find(strcmp(metric_names,'SharpnessNormed'));
precision_index = find(strcmp(metric_names,'Precision'));
decision_rate_index = find(strcmp(metric_names,'DecisionRateNorm'));
decision_time_index = find(strcmp(metric_names,'DecisionTimeNorm'));
phi_index = find(strcmp(metric_names,'Phi'));
affinity_index = find(strcmp(metric_names,'AffinityVec'));
wrong_sharp_index = find(strcmp(metric_names,'CWSharpness'));


% set sim options
sweep_options = {'n_sim',1,'n_seeds',5,'n_iters_max',50,'nStates',nStates};

%% %%%%%%%%%%%%%%%% info rate vs energy flux per cycle %%%%%%%%%%%%%%%%%%%%
tic
[sim_info_neq_0, sim_struct_neq_0] = param_sweep_multi_v2([sharpness_norm_index wrong_sharp_index],sweep_options{:},...
                                          'half_max_flag',false,'equilibrium_flag',false,'cw',1,'specFactor',100,'numCalcFlag',1);
                                        
                                        
%%                                        
[sim_info_neq_1, sim_struct_neq_1] = param_sweep_multi_v2([phi_index decision_rate_index],sweep_options{:},...
                                          'half_max_flag',false,'equilibrium_flag',false,'cw',1e4,'specFactor',100);                                        
toc     
%% 
tic
[sim_info_neq, sim_struct_neq] = param_sweep_multi_v2([decision_rate_index cw_sharpness_index],sweep_options{:},...
                                          'half_max_flag',false,'wrongFactorConcentration',1,'equilibrium_flag',false);                                        
toc                                                          
%%

[sim_info_spec_eq, sim_struct_spec_eq] = param_sweep_multi([sharpness_index spec_index],sweep_options{:},...
                                          'half_max_flag',true,'wrongFactorConcentration',1,'equilibrium_flag',true);
%%                                        
[sim_info_spec_neq, sim_struct_spec_neq] = param_sweep_multi([sharp_right_norm_index spec_index],sweep_options{:},...
                                          'half_max_flag',true,'wrongFactorConcentration',1,'equilibrium_flag',false,'testingFlag',1); 
                                        
%%                                        
                           
sharp_ind = find(sim_struct_spec_neq(2).metric_array(:,sharpness_index)>=.48&sim_struct_spec_neq(2).metric_array(:,spec_index)>=0,1);      
sharp_rates = sim_struct_spec_neq(2).rate_array(sharp_ind,:);
valMatSharp = [1 sharp_rates];
valCellSharp = mat2cell(valMatSharp,size(valMatSharp,1),ones(1,size(valMatSharp,2)));
state_probs_sharp = steadyStateVecFunction(valCellSharp{:})

spec_ind = find(sim_struct_spec_neq(2).metric_array(:,sharpness_index)>=.22&sim_struct_spec_neq(2).metric_array(:,spec_index)>=1.99,1);                                                                                
spec_rates = sim_struct_spec_neq(2).rate_array(spec_ind,:);                                        
valMatSpec = [1 spec_rates];
valCellSpec = mat2cell(valMatSpec,size(valMatSpec,1),ones(1,size(valMatSpec,2)));                                        
state_probs_spec = steadyStateVecFunction(valCellSpec{:})                                        
                                        
                                        
                                        
%%        
cr_vec = logspace(0,5,10);
info_cell_eq = cell(size(cr_vec));
info_cell_neq = cell(size(cr_vec));

for i = 1:length(cr_vec)
    tic
    
    [sim_info_spec_eq, sim_struct_spec_eq] = param_sweep_multi([affinity_index decision_rate_index],sweep_options{:},...
                                              'half_max_flag',true,'wrongFactorConcentration',cr_vec(i),'equilibrium_flag',true);

    [sim_info_spec_neq, sim_struct_spec_neq] = param_sweep_multi([affinity_index decision_rate_index],sweep_options{:},...
                                          'half_max_flag',true,'wrongFactorConcentration',cr_vec(i),'equilibrium_flag',false); 
                                        
    % find best performing eq and noneq networks
    metric_array_eq = vertcat(sim_struct_spec_eq.metric_array);   
    info_vec_eq = metric_array_eq(:,decision_rate_index);
    [~,info_mi_eq] = nanmax(info_vec_eq);
    info_cell_eq{i} = info_vec_eq(info_mi_eq);
    
    metric_array_neq = vertcat(sim_struct_spec_neq.metric_array);   
    info_vec_neq = metric_array_neq(:,decision_rate_index);
    [~,info_mi_neq] = nanmax(info_vec_neq);
    info_cell_neq{i} = info_vec_neq(info_mi_neq);

    toc
end
%%
id_cell_eq = cell(size(info_cell_neq));
id_cell_neq = cell(size(info_cell_neq));

for i = 1:length(id_cell_eq)
    id_cell_eq{i} = repelem(cr_vec(i),length(info_cell_eq{i}))';
    id_cell_neq{i} = repelem(cr_vec(i),length(info_cell_neq{i}))';
end

close all
figure;
scatter(vertcat(id_cell_eq{:}),vertcat(info_cell_eq{:}))
hold on
scatter(vertcat(id_cell_neq{:}),vertcat(info_cell_neq{:}))
%%
% [sim_info_neq1, sim_struct_neq1] = param_sweep_multi([switch_noise_index spec_index],sweep_options{:},...
%                                                             'wrongFactorConcentration',100,'equilibrium_flag',false);
                                                          
% [sim_info_eq, sim_struct_eq] = param_sweep_multi([sharpness_index spec_index],sweep_options{:},...
%                                                             'half_max_flag',false,'equilibrium_flag',true);                                                          
%                                                           
% [sim_info_eq, sim_struct_eq] = param_sweep_multi([flux_index sharp_spec_index],sweep_options{:},...
%                                                             'half_max_flag',false,'equilibrium_flag',true);                                                          
                                                          
toc                                                          

% [sim_info_neq_half, sim_struct_neq_half] = param_sweep_multi([flux_index decision_rate_index],sweep_options{:},'half_max_flag',true);
% toc
% 
% % set save name
% save_name_flux = ['param_sweep_results_' metric_names{flux_index} '_' ...
%     metric_names{decision_rate_index}];
% save([OutPath save_name_flux '_eq0.mat'],'sim_struct_neq','-v7.3')
% save([OutPath save_name_flux 'info_eq0.mat'],'sim_info_neq')
% 
% save([OutPath save_name_flux '_half_eq0.mat'],'sim_struct_neq','-v7.3')
% save([OutPath save_name_flux '_half_info_eq0.mat'],'sim_info_neq')

%% %%%%%%%%%%%%%%%% info rate vs "affinity" (i.e. log(koff/kon) ) %%%%%%%%%
tic
[sim_info_neq, sim_struct_neq]  = param_sweep_multi([affinity_index decision_rate_index],sweep_options{:},'equilibrium_flag',false);

[sim_info_eq, sim_struct_eq]  = param_sweep_multi([affinity_index decision_rate_index],sweep_options{:},'equilibrium_flag',true);
% sim_struct_neq = param_sweep_multi([affinity_index decision_rate_index],sweep_options{:},'equilibrium_flag',false,'n_seeds',50);
toc

% set save name
% save_name_affinity = ['param_sweep_results_' metric_names{affinity_index} '_' ...
%     metric_names{decision_rate_index}];
% save([OutPath save_name_affinity '_eq0.mat'],'sim_struct_neq','-v7.3')
% save([OutPath save_name_affinity 'info_eq0.mat'],'sim_info_neq')
% save([OutPath save_name_affinity '_eq1.mat'],'sim_struct_eq','-v7.3')
% save([OutPath save_name_affinity 'info_eq1.mat'],'sim_info_eq')

%% %%%%%%%%%%%%%%%% decision time vs "affinity" (i.e. log(koff/kon) ) %%%%%%%%%
tic
[sim_info_neq, sim_struct_neq] = param_sweep_multi([affinity_index decision_time_index],sweep_options{:},'equilibrium_flag',false);

[sim_info_eq, sim_struct_eq] = param_sweep_multi([affinity_index decision_time_index],sweep_options{:},'equilibrium_flag',true);
% sim_struct_neq = param_sweep_multi([affinity_index decision_rate_index],sweep_options{:},'equilibrium_flag',false,'n_seeds',50);
toc

% set save name
% save_name_affinity = ['param_sweep_results_' metric_names{affinity_index} '_' ...
%     metric_names{decision_time_index}];
% save([OutPath save_name_affinity '_eq0.mat'],'sim_struct_neq','-v7.3')
% save([OutPath save_name_affinity 'info_eq0.mat'],'sim_info_neq')
% save([OutPath save_name_affinity '_eq1.mat'],'sim_struct_eq','-v7.3')
% save([OutPath save_name_affinity 'info_eq1.mat'],'sim_info_eq')


%% %%%%%%%%%%%%%%%% sharpness vs precision %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
[sim_info_neq, sim_struct_neq] = param_sweep_multi([sharpness_index precision_index],sweep_options{:},...
                        'equilibrium_flag',false,'half_max_flag',false);
toc

tic
[sim_info_eq, sim_struct_eq] = param_sweep_multi([sharpness_index precision_index],sweep_options{:},...
                        'equilibrium_flag',true,'half_max_flag',false);
toc

% set save name
save_name_tradeoff = ['param_sweep_results_' metric_names{sharpness_index} '_' ...
                                              metric_names{precision_index}];
save([OutPath save_name_tradeoff '_eq0.mat'],'sim_struct_neq','-v7.3')
save([OutPath save_name_tradeoff 'info_eq0.mat'],'sim_info_neq')
save([OutPath save_name_tradeoff '_eq1.mat'],'sim_struct_eq','-v7.3')
save([OutPath save_name_tradeoff 'info_eq1.mat'],'sim_info_eq')

%% %%%%%%%%%%%%%%%% sharpness vs switching noise %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plot_indices = [sharpness_index switch_noise_index];
tic
[sim_info_neq, sim_struct_neq] = param_sweep_multi(plot_indices, sweep_options{:},...
                        'equilibrium_flag',false,'half_max_flag',true);
toc

tic
[sim_info_eq, sim_struct_eq] = param_sweep_multi(plot_indices,sweep_options{:},...
                        'equilibrium_flag',true,'half_max_flag',true);
toc

% set save name
save_name_tradeoff = ['param_sweep_results_' metric_names{plot_indices(1)} '_' ...
                      metric_names{plot_indices(2)}];
save([OutPath save_name_tradeoff '_eq0.mat'],'sim_struct_neq','-v7.3')
save([OutPath save_name_tradeoff 'info_eq0.mat'],'sim_info_neq')
save([OutPath save_name_tradeoff '_eq1.mat'],'sim_struct_eq','-v7.3')
save([OutPath save_name_tradeoff 'info_eq1.mat'],'sim_info_eq')

%% %%%%%%%%%%% sharpness vs precision (with half-max constraint) %%%%%%%%%%
plot_indices = [sharpness_index switch_noise_index];

tic
[sim_info_neq, sim_struct_neq] = param_sweep_multi(plot_indices,sweep_options{:},...
                        'equilibrium_flag',false,'half_max_flag',true);
toc

tic
[sim_info_eq, sim_struct_eq] = param_sweep_multi(plot_indices,sweep_options{:},...
                        'equilibrium_flag',true,'half_max_flag',true);
toc

% set save name
save_name_tradeoff = ['param_sweep_results_' metric_names{plot_indices(1)} '_' ...
                      metric_names{plot_indices(2)}];
save([OutPath save_name_tradeoff '_half_eq0.mat'],'sim_struct_neq','-v7.3')
save([OutPath save_name_tradeoff '_halfinfo_eq0.mat'],'sim_info_neq')
save([OutPath save_name_tradeoff '_half_eq1.mat'],'sim_struct_eq','-v7.3')
save([OutPath save_name_tradeoff '_halfinfo_eq1.mat'],'sim_info_eq')
