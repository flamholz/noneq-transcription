% script to call core parameter sweep function to examine tradeoffs between
% different network behaviors

clear 
close all
addpath(genpath('../utilities/'))


[~,metric_names] = calculateMetricsNumeric([]);
nStates = 18;
% make sure we're linked to the appropriate function subfolder
rmpath(genpath('../utilities/metricFunctions/'));
addpath(genpath(['../utilities/metricFunctions/n18_OR_NUM/']));

% define save path
OutPath = ['../../out/bivariate_parameter_sweeps_n18_numeric' filesep];
mkdir(OutPath);
                         

% get index of useful metrics
flux_index = find(strcmp(metric_names,'Flux'));
rate_index = find(strcmp(metric_names,'Production Rate'));
spec_index = find(strcmp(metric_names,'Specificity'));
spec_alt_index = find(strcmp(metric_names,'specFactorAlt'));
dev_factor_index = find(strcmp(metric_names,'deviationFactor'));
sharp_right_index = find(strcmp(metric_names,'SharpnessRight'));
sharp_right_norm_index = find(strcmp(metric_names,'SharpnessRightNorm'));
sharpness_index = find(strcmp(metric_names,'Sharpness'));
sharpness_norm_index = find(strcmp(metric_names,'SharpnessNormed'));
precision_index = find(strcmp(metric_names,'Precision'));
decision_rate_index = find(strcmp(metric_names,'DecisionRateNorm'));
decision_time_index = find(strcmp(metric_names,'DecisionTimeNorm'));
phi_index = find(strcmp(metric_names,'Phi'));
affinity_index = find(strcmp(metric_names,'AffinityVec'));
wrong_sharp_index = find(strcmp(metric_names,'CWSharpness'));


% set sim options
sweep_options = {'n_sim',1,'n_seeds',5,'n_iters_max',50,'nStates',nStates};

%% %%%%%%%%%%%%%%%% info rate vs energy flux per cycle %%%%%%%%%%%%%%%%%%%%
sweepFlags0 = [0 0 0 1 1 1 1 1 1 1 1 1 1 1 1]==1;
sweepFlags1 = sweepFlags0;
sweepFlags1(end-3:end) = false;
spec1 = 100;
cw1 = 1;

tic
[sim_info_neq_0, sim_struct_neq_0] = param_sweep_multi_v2([sharp_right_norm_index wrong_sharp_index],sweep_options{:},...
                                          'half_max_flag',false,'equilibrium_flag',false,'cw',cw1,...
                                          'sweepFlags',sweepFlags1,'specFactor',spec1,'twoSpecFlag',0);
                                        
% [~, sim_struct_neq_1] = param_sweep_multi_v2([phi_index decision_rate_index],sweep_options{:},...
%                                           'half_max_flag',false,'equilibrium_flag',false,'cw',cw1,...
%                                           'sweepFlags',sweepFlags1,'specFactor',spec1,'twoSpecFlag',0);                                        
toc    

%%
sweepFlags0 = [0 0 0 1 1 1 1 1 1 1 1 1 1 1 1]==1;
sweepFlags0(end-3:end) = false;
spec2 = 2;
tic
[sim_info_neq2, sim_struct_neq2] = param_sweep_multi_prob([rate_index sharpness_index],sweep_options{:},...
                                          'half_max_flag',false,'equilibrium_flag',false,'cw',cw1,...
                                          'sweepFlags',sweepFlags0,'specFactor',spec2);
toc   
