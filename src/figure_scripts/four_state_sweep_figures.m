% Plot results of sharpness parameter sweeps
clear 
close all
addpath(genpath('../utilities/'))

nStates = 4;
rmpath(genpath('../utilities/metricFunctions/'));
addpath(['../utilities/metricFunctions/n' num2str(nStates) '_OR/']);

% %%%%%%%%%%%%%%%%  set relative read and write paths %%%%%%%%%%%%%%%%%%%%
DataPath = ['../../out/bivariate_parameter_sweeps_n4_OR' filesep];
DropboxFolder = 'C:\Users\nlamm\Dropbox (Personal)\Nonequilibrium\Nick\manuscript\';
FigPath = [DropboxFolder 'fourStateSweeps' filesep];
mkdir(FigPath);

% %%%%%%%%%%%%%%%%  get metric names %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[~,metric_names] = calculateMetricsMultiState([]);
flux_index = find(strcmp(metric_names,'Flux'));
rate_index = find(strcmp(metric_names,'Production Rate'));
sharpness_index = find(strcmp(metric_names,'Sharpness'));
precision_index = find(strcmp(metric_names,'Precision'));
decision_rate_index = find(strcmp(metric_names,'DecisionRateNorm'));
decision_time_index = find(strcmp(metric_names,'DecisionTimeNorm'));
cycle_time_index = find(strcmp(metric_names,'CycleTime'));
phi_index = find(strcmp(metric_names,'Phi'));

% %%%%%%%%%%%%%%%%  Set plot parameters and constants %%%%%%%%%%%%%%%%%%%%

n_plot = 3e3; % number of points to plot
markerAlpha = 0.5; % marker transparency
markerSize = 75; % marker size

% set sweep options
sweep_options = {'n_seeds',5,'n_iters_max',50};

%% %%%% Plot Phi versus vs IR %%%%%%%%%%

% run sweep
[sim_info_neq, sim_struct_neq] = param_sweep_multi([phi_index decision_rate_index],sweep_options{:},...
                                                            'half_max_flag',true,'equilibrium_flag',false,'sameOnRateFlag',0);                                             
c_factor = (0.1/1)^2;

% calculate maximum info rates
metric_array_flux = vertcat(sim_struct_neq.metric_array);   
phi_vec = metric_array_flux(:,phi_index);
flux_vec = metric_array_flux(:,flux_index);
info_vec = metric_array_flux(:,decision_rate_index) * log2(exp(1));% ./ c_factor;
plot_filter = flux_vec>=0;% & precision_vec >=0;
plot_indices = randsample(find(plot_filter),n_plot,true,info_vec(plot_filter));

% make figures

info_vs_flux = figure;
cmap = brewermap(8,'Purples');
hold on

scatter(phi_vec(plot_indices),info_vec (plot_indices),...
      markerSize,'MarkerEdgeAlpha',.25,'MarkerEdgeColor','k','MarkerFaceAlpha',markerAlpha, 'MarkerFaceColor',cmap(5,:)); 

% ylim([2 4.25])
xlim([1e-2 3e1])
    
xlabel('\Phi (k_bT per cycle)');
ylabel('IR (bits per cycle)')
grid on
set(gca,'FontSize',14)
set(gca,'Color',[228,221,209]/255) 

ax = gca;
ax.YAxis(1).Color = 'k';
ax.XAxis(1).Color = 'k';

info_vs_flux.InvertHardcopy = 'off';
set(gcf,'color','w');
set(gca,'xscale','log')

saveas(info_vs_flux,[FigPath 'info_vs_flux.png'])
saveas(info_vs_flux,[FigPath 'info_vs_flux.pdf'])                                                                                                                                                                                                                                                                                                                       
                                                          
%% Plot upper info bound, along with corresponding sharpness and precision
n_points = 30;
phi_bins = logspace(-2, log10(30),n_points+1);
prec_vec = exp(metric_array_flux(:, precision_index)).^2;
sharp_vec = metric_array_flux(:, sharpness_index);
sharp_filter = sharp_vec >= 0;
i_max = [];
s_max = [];
p_max = [];
phi_axis = [];

for p = 1:n_points
    phi_filter = phi_vec >= phi_bins(p) & phi_vec < phi_bins(p+1) & sharp_filter;
    i_98 = prctile(info_vec(phi_filter),98);
    i_98_filter = info_vec.*phi_filter >= i_98;
    
    i_max = [i_max mean(info_vec(i_98_filter)')];
    s_max = [s_max mean(sharp_vec(i_98_filter)')];
    p_max = [p_max mean(prec_vec(i_98_filter)')];
    phi_axis = [phi_axis repelem((phi_bins(p)+phi_bins(p+1))/2, 1)];
end

% sepcify eq avlues
i_eq = 1/4 / 100 * log2(exp(1));
s_eq = 0.25;
p_eq = 8;
cmap = brighten(brewermap([],'Set2'),-0.25);

close all


opt_fig = figure;

hold on
plot(phi_axis, i_max / i_eq, 'Color', 'k', 'LineWidth',1.5)%'MarkerEdgeAlpha', 1, 'MarkerEdgeColor', ...
                        %'k', 'MarkerFaceAlpha', 0.5, 'MarkerFaceColor',cmap(5,:))
                      
plot(phi_axis,(s_max / s_eq).^2, '--*','Color',cmap(5,:),'LineWidth',1.5);%0.75*markerSize,'s','MarkerEdgeAlpha', 0.25, 'MarkerEdgeColor', ...
                        %'k', 'MarkerFaceAlpha', 1, 'MarkerFaceColor',cmap(5,:))
                      
plot(phi_axis,(p_max / p_eq), '-s','Color',cmap(2,:),'LineWidth',1.5);%, 0.75*markerSize,'^','MarkerEdgeAlpha', 0.25, 'MarkerEdgeColor', ...
                        %'k', 'MarkerFaceAlpha', 1, 'MarkerFaceColor',cmap(6,:))
set(gca,'xscale','log')

xlim([1e-2 3e1])
ylim([0 4.5])

xlabel('\Phi (k_bT per cycle)');
ylabel('non-equilibrium gain')

legend('IR/IR_{eq}','s^2/s_{eq}^2','\sigma_{eq}^2/\sigma^2','Location','northwest')

grid on
set(gca,'FontSize',14)
set(gca,'Color',[228,221,209]/255) 

ax = gca;
ax.YAxis(1).Color = 'k';
ax.XAxis(1).Color = 'k';

opt_fig.InvertHardcopy = 'off';
set(gcf,'color','w');
set(gca,'xscale','log')

saveas(opt_fig,[FigPath 'opt_vs_flux.png'])
saveas(opt_fig,[FigPath 'opt_vs_flux.pdf'])  

%% Plot eq and neq decision time bounds
% close all
t_cycle = 5*60; % set the cycle time
n_lines = 10;
error_vec = logspace(log10(0.4999),-2);
c1 = 1.05;
c0 = 0.95;
% generate vector of energy levels
phi_filter_vec = logspace(-2,1,n_lines);
rate_array_flux = vertcat(sim_struct_neq.rate_array); 

% initialize arrays to store rates
rate_array = NaN(n_lines,size(rate_array_flux,2));
tau_array = NaN(length(error_vec),n_lines);
tau_array_alt = NaN(length(error_vec),n_lines);

% iterate
for i = 1:n_lines
  if i == 1
    phi_filter = 0 <=phi_vec&phi_vec < phi_filter_vec(i); 
  else
    phi_filter = phi_filter_vec(i-1) <= phi_vec&phi_vec < phi_filter_vec(i); 
  end
    
%   [eq_max, optimal_eq_index] = max(info_vec.*(phi_filter&plot_filter));
  [neq_max, optimal_neq_index] = max(info_vec.*(phi_filter&plot_filter));

  % extract cycle times
%   cycle_time_eq = metric_array_flux(optimal_eq_index,cycle_time_index); 
  cycle_time = metric_array_flux(optimal_neq_index,cycle_time_index);

% [V,T,R] = calculateDecisionMetrics(rate_array,c1,c_val,minError);
%   optimal_eq_rates = rate_array_flux(optimal_eq_index,:)*cycle_time_eq/t_cycle;

  optimal_rates = rate_array_flux(optimal_neq_index,:);

  for e = 1:length(error_vec)
      [VNeq,tau_array(e,i),~] = calculateDecisionMetrics(optimal_rates,c1,c0,error_vec(e));
%       [VEq,tau_vec_eq(e),~] = calculateDecisionMetrics(optimal_eq_rates,c1,c0,error_vec(e));      
  end
  tau_array_alt(:,i) = log((1-error_vec)./error_vec) .*(1-2.*error_vec)./VNeq;
end
  
%%
close all
decision_time_plot = figure('Position',[100 100 560*1.15  420]);
cmap = flipud(brewermap(n_lines,'Spectral'));
colormap(cmap);

hold on
for i = 1:n_lines
    plot(error_vec,tau_array(:,i)*t_cycle/60,'Color',cmap(i,:),'LineWidth',2); 
end
% plot(error_vec,tau_vec_eq/60,'Color',cmap(3,:),'LineWidth',3); 

% legend('nonequilibrium limit','equilibrium limit')    
xlabel('error tolerance (\epsilon)');
ylabel('\langle T \rangle (minutes)');
set(gca,'xdir','reverse')

h = colorbar;
h.Ticks = [1 4 7 10]/10;
h.TickLabels = {'10^{-2}','10^{-1}','10^{0}','10^{1}'};
h.Color = 'k';
ylabel(h,'\Phi (k_bT per cycle)')
% set(gca,'xtick',[0.05 0.15 
grid on
set(gca,'FontSize',14)
set(gca,'Color',[228,221,209]/255) 

ax = gca;
ax.YAxis(1).Color = 'k';
ax.XAxis(1).Color = 'k';

ylim([1e2 1e4])
xlim([0 .35])
set(gca,'yscale','log')
set(gca,'xscale','log')

decision_time_plot.InvertHardcopy = 'off';
set(gcf,'color','w');
saveas(decision_time_plot,[FigPath 'decision_time_plot.png'])
saveas(decision_time_plot,[FigPath 'decision_time_plot.pdf'])

%% %%%%%%%%%%%%%%%% Plot Sharpness vs Precision %%%%%%%%%%%%%%%%
% run sweep
tic
[sp_sim_info_neq, sp_sim_struct_neq] = param_sweep_multi([sharpness_index precision_index],sweep_options{:},...
                                                            'half_max_flag',true,'equilibrium_flag',false,'sameOnRateFlag',0);
                                                          
[sp_sim_info_eq, sp_sim_struct_eq] = param_sweep_multi([sharpness_index precision_index],sweep_options{:},...
                                                            'half_max_flag',true,'equilibrium_flag',true,'sameOnRateFlag',0);
toc                                                          
                                                          

% process neq data
metric_array_neq = vertcat(sp_sim_struct_neq.metric_array);   
sharpness_vec_neq = metric_array_neq(:,sharpness_index).^2;
precision_vec_neq = exp(metric_array_neq(:,precision_index)).^2;
plot_filter_neq = sharpness_vec_neq>=0;% & precision_vec >=0;
plot_indices_neq = randsample(find(plot_filter_neq),n_plot,true);

% process eq data
metric_array_eq = vertcat(sp_sim_struct_eq.metric_array);   
sharpness_vec_eq = metric_array_eq(:,sharpness_index).^2;
precision_vec_eq = exp(metric_array_eq(:,precision_index)).^2;
plot_filter_eq = sharpness_vec_eq>=0;% & precision_vec >=0;
plot_indices_eq = randsample(find(plot_filter_eq),n_plot,true);


cmap = brewermap(8,'Set2');
for i = 1:2
  
    precision_vs_sharpness = figure;
    
    hold on
    if i == 2
        scatter(sharpness_vec_neq(plot_indices_neq),precision_vec_neq(plot_indices_neq),...
              markerSize,'MarkerEdgeAlpha',.25,'MarkerEdgeColor','k','MarkerFaceAlpha',markerAlpha, 'MarkerFaceColor',cmap(2,:)); 
        sneq = scatter(0,0,75,'MarkerEdgeColor','k', 'MarkerFaceColor',cmap(2,:));
    end
    scatter(sharpness_vec_eq(plot_indices_eq),precision_vec_eq(plot_indices_eq),...
          markerSize,'MarkerEdgeAlpha',.25,'MarkerEdgeColor','k','MarkerFaceAlpha',markerAlpha, 'MarkerFaceColor',cmap(3,:));     
    seq = scatter(0,0,75,'MarkerEdgeColor','k', 'MarkerFaceColor',cmap(3,:));

    ylim([0 16.5])
    xlim([0 0.25])
        
    if i == 2
        legend([sneq seq],'nonequilibrium networks','equilibrium networks','Location','northeast')
    else
        legend([seq],'equilibrium networks','Location','northeast')
    end
    xlabel('sharpness factor (dr/dc)^2');
    ylabel('precision (\tau_c/\sigma_r^2)')
    grid on
    set(gca,'FontSize',14)
    set(gca,'Color',[228,221,209]/255) 

    ax = gca;
    ax.YAxis(1).Color = 'k';
    ax.XAxis(1).Color = 'k';

    precision_vs_sharpness.InvertHardcopy = 'off';
    set(gcf,'color','w');

    if i == 2          
        saveas(precision_vs_sharpness,[FigPath 'precision_vs_sharpness.png'])
        saveas(precision_vs_sharpness,[FigPath 'precision_vs_sharpness.pdf'])
    else
        saveas(precision_vs_sharpness,[FigPath 'precision_vs_sharpness_eq.png'])
        saveas(precision_vs_sharpness,[FigPath 'precision_vs_sharpness_eq.pdf'])
    end
end

%% Look at motifs
close all
ppl = [212 200 227]/256;

% process neq data
rate_array_neq = vertcat(sp_sim_struct_neq.rate_array);   
rate_array_eq = vertcat(sp_sim_struct_eq.rate_array);   
tau_cycle_vec = metric_array_neq(:,cycle_time_index);
tau_cycle_vec_eq = metric_array_eq(:,cycle_time_index);
info_vec = metric_array_neq(:,decision_rate_index);
info_vec_eq = metric_array_eq(:,decision_rate_index);
rate_array_neq = rate_array_neq ./ tau_cycle_vec;
rate_array_eq = rate_array_eq ./ tau_cycle_vec_eq;

% filter for reasonable scales...this needs to be revisited bur both bounds
% are met even when applying these filteres
r_filter = all(rate_array_neq>=1e-4&rate_array_neq<=1e4,2);
r_filter_eq = all(rate_array_eq>=1e-4&rate_array_eq<=1e4,2);

% find maxima
[sm,si] = max(sharpness_vec_neq.*r_filter);
% s_99 = prctile(sharpness_vec_neq(r_filter&sharpness_vec_neq>0),99);
[pm,pi] = max(precision_vec_neq.*r_filter);
[im,ii] = max(info_vec.*r_filter);
[im_eq,ii_eq] = max(info_vec_eq.*r_filter_eq);


%%
% generate rate labels 
label_cell = {'k_-^i','k_{off}^-','k_+^i','k_{off}^+','k_{on}^+','k_+^a','k_{on}^-','k_-^a'};
clockwise_flags = [0 1 1 0 1 0 0 1]==1;
order = [1 3 4 5 6 8 7 2];
clockwise_flags = clockwise_flags(order);

s_bar = figure('Position',[100 100 512 200]);

s_rates = rate_array_neq(si,order);
hold on
bar(find(clockwise_flags),s_rates(clockwise_flags),0.45,'FaceColor',brighten(ppl,0.4))
bar(find(~clockwise_flags),s_rates(~clockwise_flags),0.45,'FaceColor',brighten(ppl,-0.4))

set(gca,'yscale','log')
set(gca,'xtick',1:8)
set(gca,'xticklabels',label_cell(order))

% xlabel('edge labels')
ylabel('events per second')
% legend('cw rates','ccw rates','Location','northwest')
grid on
set(gca,'FontSize',14)
set(gca,'Color',[228,221,209]/255) 

ax = gca;
ax.YAxis(1).Color = 'k';
ax.XAxis(1).Color = 'k';
set(gca,'ytick',[1e-4 1 1e4])
ylim([1e-4 1e4])
xlim([0 9])
box on
s_bar.InvertHardcopy = 'off';
set(gcf,'color','w');
saveas(s_bar,[FigPath 'tr_rates_sharp.png'])
saveas(s_bar,[FigPath 'tr_rates_sharp.pdf'])

%%%%%%%%%%%%%%
%% precision

p_bar = figure('Position',[100 100 512 200]);
p_rates = rate_array_neq(pi,order);
hold on
bar(find(clockwise_flags),p_rates(clockwise_flags),0.45,'FaceColor',brighten(ppl,0.4))
bar(find(~clockwise_flags),p_rates(~clockwise_flags),0.45,'FaceColor',brighten(ppl,-0.4))

set(gca,'yscale','log')
set(gca,'xtick',1:8)
set(gca,'xticklabels',label_cell(order))

% xlabel('edge labels')
ylabel('events per second')
% legend('cw rates','ccw rates','Location','northwest')
grid on
set(gca,'FontSize',14)
set(gca,'ytick',[1e-4 1 1e4])
set(gca,'Color',[228,221,209]/255) 

box on
ax = gca;
ax.YAxis(1).Color = 'k';
ax.XAxis(1).Color = 'k';

ylim([1e-4 1e4])
xlim([0 9])

p_bar.InvertHardcopy = 'off';
set(gcf,'color','w');

saveas(p_bar,[FigPath 'tr_rates_prec.png'])
saveas(p_bar,[FigPath 'tr_rates_prec.pdf'])
%% Sharpness vs Phi 
% run sweep
tic
[sharp_sim_info_neq, sharp_sim_struct_neq] = param_sweep_multi([sharpness_index decision_rate_index],sweep_options{:},...
                                                            'half_max_flag',true,'equilibrium_flag',false);
[prec_sim_info_neq, prec_sim_struct_neq] = param_sweep_multi([precision_index decision_rate_index],sweep_options{:},...
                                                            'half_max_flag',true,'equilibrium_flag',false);
toc
%% calculate maximum info rates
metric_array_sharp = vertcat(sharp_sim_struct_neq.metric_array); 
tau_cycle_vec_sharp = metric_array_sharp(:,cycle_time_index);
phi_vec_sharp = metric_array_sharp(:,phi_index);
flux_vec_sharp = metric_array_sharp(:,flux_index);
sharpness_vec = metric_array_sharp(:,sharpness_index);% ./ c_factor;

metric_array_prec = vertcat(prec_sim_struct_neq.metric_array);
precision_vec = exp(metric_array_sharp(:,precision_index)).^2;
phi_vec_prec = metric_array_prec(:,phi_index);
% info_vec = metric_array_flux(:,decision_rate_index);% ./ c_factor;
% sharpness_vec = metric_array_flux(:,sharpness_index);
% plot_filter = sharpness_vec>=0;% & precision_vec >=0;
% plot_indices = randsample(find(plot_filter),n_plot,true,sharpness_vec(plot_filter));

% make figures


%%
scatter(phi_vec(plot_indices),sharpness_vec (plot_indices),...
      markerSize,'MarkerEdgeAlpha',.25,'MarkerEdgeColor','k','MarkerFaceAlpha',markerAlpha, 'MarkerFaceColor',cmap(5,:)); 
    
% ylim([2 4.25])
xlim([1e-6 3e1])
    
xlabel('\Phi (k_bT per cycle)');
ylabel('sharpness (c*dr/dc)')
grid on
set(gca,'FontSize',14)
set(gca,'Color',[228,221,209]/255) 

ax = gca;
ax.YAxis(1).Color = 'k';
ax.XAxis(1).Color = 'k';

info_vs_flux.InvertHardcopy = 'off';
set(gcf,'color','w');
set(gca,'xscale','log')

%%
saveas(sharpness_vs_flux,[FigPath 'sharpness_vs_flux.png'])
saveas(sharpness_vs_flux,[FigPath 'sharpness_vs_flux.pdf'])     
