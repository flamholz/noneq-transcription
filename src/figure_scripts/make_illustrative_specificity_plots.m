% script to generate illustrative log likelihood plots
clear
close all
addpath(genpath('../utilities'))

% make figure path
DropboxFolder = 'C:\Users\nlamm\Dropbox (Personal)\Nonequilibrium\Nick\manuscript\Specificity\';
mkdir(DropboxFolder)

%% Do parameter sweeps
% set basic parameters
nStates = 6;
rate_bounds = repmat([-6 ; 6],1,3*nStates-4); % constrain transition rate magnitude
[~,metric_names] = calculateMetricsMultiState([]);

% make sure we're linked to the appropriate function subfolder
rmpath(genpath('../utilities/metricFunctions/'));
addpath(genpath(['../utilities/metricFunctions/n' num2str(nStates) '_OR/']));

% get index of useful metrics
flux_index = find(strcmp(metric_names,'Flux'));
rate_index = find(strcmp(metric_names,'Production Rate'));
spec_index = find(strcmp(metric_names,'Specificity'));
spec_alt_index = find(strcmp(metric_names,'specFactorAlt'));
affinity_index = find(strcmp(metric_names,'AffinityVec'));

% set sim options
sweep_options = {'n_seeds',5,'n_iters_max',50,'nStates',nStates};

% sweep parallel specificity definition
[sim_info_eq, sim_struct_eq] = param_sweep_multi([affinity_index spec_alt_index], sweep_options{:},...
                                          'half_max_flag',true,'wrongFactorConcentration',1,'equilibrium_flag',true);

[sim_info_neq, sim_struct_neq] = param_sweep_multi([affinity_index spec_index], sweep_options{:},...
                                          'half_max_flag',true,'wrongFactorConcentration',1,'equilibrium_flag',false);

%% %%%%%%%%%%%%%%%%%%%%%%% define core parameters %%%%%%%%%%%%%%%%%%%%%%%%

[affinity_vec, si] = sort(10.^sim_struct_eq(3).metric_array(:,affinity_index));

spec_parallel_vec = 10.^sim_struct_eq(3).metric_array(:,spec_alt_index);
spec_parallel_vec = imgaussfilt(spec_parallel_vec(si),10);

spec_same_vec = 10.^sim_struct_eq(3).metric_array(:,spec_index);
spec_same_vec = imgaussfilt(spec_same_vec(si),10);

plot_indices = sort(randsample(1:length(spec_same_vec),1e3,false));

[affinity_vec_neq, si_neq] = sort(10.^sim_struct_neq(3).metric_array(:,affinity_index));
spec_same_vec_neq = 10.^sim_struct_neq(3).metric_array(:,spec_index);
spec_same_vec_neq = spec_same_vec_neq(si_neq);

neq_aff_vec = logspace(-4,2,101); 
aff_axis = 10.^(log10(neq_aff_vec(1:end-1)) + diff(log10(neq_aff_vec)));
neq_spec_vec = NaN(size(aff_axis));

for i = 1:length(neq_aff_vec)-1
    lb = neq_aff_vec(i);
    ub = neq_aff_vec(i+1);
    neq_spec_vec(i) = nanmax(spec_same_vec_neq(affinity_vec_neq>=lb & affinity_vec_neq < ub));
end

purple = [212 200 227]/255;

close all

specificity_fig = figure;
cmap = brewermap([],'Set2');
hold on

plot(affinity_vec(plot_indices),spec_parallel_vec(plot_indices),'Color','k','LineWidth',3)
plot(affinity_vec(plot_indices),spec_same_vec(plot_indices),'Color',cmap(3,:),'LineWidth',3)
plot(aff_axis,neq_spec_vec,'-.','Color',cmap(2,:),'LineWidth',3)

legend('f_p (equilibrium)','f_0 (equilibrium)','f_0 (non-eq bound)','Location','southeast')
set(gca,'FontSize',14)
%     set(gca, 'xtick', [],'ytick', [])
xlabel('activator affinity (k_{off}/k_{on})')
ylabel('activator fidelity (f_0/\beta)')
set(gca,'Color',[228,221,209]/255) 
ax = gca;

% ax.XAxis.MinorTickValues = 10.^(-5:.1:5);
ax.YAxis(1).Color = 'k';
ax.XAxis(1).Color = 'k';
specificity_fig.InvertHardcopy = 'off';
set(gcf,'color','w');
set(gca,'xscale','log')
set(gca,'yscale','log')
grid on

ylim([10^-2.5 10^2.5])
xlim([1e-3 1e1])
saveas(specificity_fig,[DropboxFolder 'compare_spec.png'])
saveas(specificity_fig,[DropboxFolder 'compare_spec.pdf'])