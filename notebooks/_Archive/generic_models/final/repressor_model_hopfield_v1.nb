(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     27201,        685]
NotebookOptionsPosition[     24239,        626]
NotebookOutlinePosition[     24585,        641]
CellTagsIndexPosition[     24542,        638]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[StyleBox["Solution to general 3 state model with forward k \
rates and reverse r rates ", "Subsection"]], "Text",
 CellChangeTimes->{{3.750384041936879*^9, 3.7503840629025908`*^9}, {
  3.7541376872592278`*^9, 3.7541376890619497`*^9}, {3.760544877224125*^9, 
  3.7605448779661417`*^9}},ExpressionUUID->"d1518c95-f8f3-413c-b038-\
17f90c0a1e60"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ass", "=", 
   RowBox[{"{", " ", 
    RowBox[{
     RowBox[{"koff", ">", "0"}], " ", ",", " ", 
     RowBox[{"km", ">", "0"}], ",", " ", 
     RowBox[{"kp", ">", "0"}], ",", " ", 
     RowBox[{"kon", ">", "0"}], ",", " ", 
     RowBox[{"c0", "<", "c1"}], ",", " ", 
     RowBox[{"c1", ">", "0"}], ",", " ", 
     RowBox[{"c0", ">", "0"}], ",", " ", 
     RowBox[{"b", " ", ">", " ", "1"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.750345019237339*^9, 3.7503450539523153`*^9}, {
  3.7541376911164703`*^9, 3.754137691796073*^9}, {3.754139973332193*^9, 
  3.754140007229397*^9}, {3.754142059571416*^9, 3.754142059771084*^9}, {
  3.754143332172714*^9, 3.75414333445453*^9}, {3.754143472486924*^9, 
  3.754143474028854*^9}, {3.7571713474989743`*^9, 3.757171356767172*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"c95437b1-8467-44ee-b3ad-5728a54b961b"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sol", " ", "=", " ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"{", 
       RowBox[{"J", ",", "J", ",", "J", ",", "1"}], "}"}], "\[Equal]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{
         RowBox[{"k1", " ", "p1"}], " ", "-", " ", 
         RowBox[{"r1", " ", "p2"}]}], ",", " ", 
        RowBox[{
         RowBox[{"k2", " ", "p2"}], " ", "-", " ", 
         RowBox[{"r2", " ", "p3"}]}], ",", " ", 
        RowBox[{
         RowBox[{"k3", " ", "p3"}], " ", "-", " ", 
         RowBox[{"r3", " ", "p1"}]}], ",", "  ", 
        RowBox[{"p1", "+", "p2", "+", "p3"}]}], "}"}]}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"J", ",", " ", "p1", ",", "p2", ",", "p3"}], "}"}]}], "]"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.7450741599696827`*^9, 3.7450741968805532`*^9}, {
   3.750343738376728*^9, 3.750343740147134*^9}, {3.750357778746254*^9, 
   3.750357778821937*^9}, {3.7541379940669727`*^9, 3.754137994633065*^9}, {
   3.754139921940263*^9, 3.7541399257643223`*^9}, {3.754142072152648*^9, 
   3.754142076472227*^9}, 3.7541445815119257`*^9, {3.760544890049841*^9, 
   3.7605449392155867`*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"7fb5bbc1-0e7f-4bb4-a355-efe1fde738a9"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"J", "\[Rule]", 
     FractionBox[
      RowBox[{
       RowBox[{"k1", " ", "k2", " ", "k3"}], "-", 
       RowBox[{"r1", " ", "r2", " ", "r3"}]}], 
      RowBox[{
       RowBox[{"r1", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{"k1", " ", 
        RowBox[{"(", 
         RowBox[{"k2", "+", "k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"r1", "+", "r2"}], ")"}], " ", "r3"}], "+", 
       RowBox[{"k2", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r3"}], ")"}]}]}]]}], ",", 
    RowBox[{"p1", "\[Rule]", 
     FractionBox[
      RowBox[{
       RowBox[{"k2", " ", "k3"}], "+", 
       RowBox[{"r1", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r2"}], ")"}]}]}], 
      RowBox[{
       RowBox[{"r1", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{"k1", " ", 
        RowBox[{"(", 
         RowBox[{"k2", "+", "k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"r1", "+", "r2"}], ")"}], " ", "r3"}], "+", 
       RowBox[{"k2", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r3"}], ")"}]}]}]]}], ",", 
    RowBox[{"p2", "\[Rule]", 
     FractionBox[
      RowBox[{
       RowBox[{"k1", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{"r2", " ", "r3"}]}], 
      RowBox[{
       RowBox[{"r1", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{"k1", " ", 
        RowBox[{"(", 
         RowBox[{"k2", "+", "k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"r1", "+", "r2"}], ")"}], " ", "r3"}], "+", 
       RowBox[{"k2", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r3"}], ")"}]}]}]]}], ",", 
    RowBox[{"p3", "\[Rule]", 
     FractionBox[
      RowBox[{
       RowBox[{"k1", " ", "k2"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"k2", "+", "r1"}], ")"}], " ", "r3"}]}], 
      RowBox[{
       RowBox[{"r1", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{"k1", " ", 
        RowBox[{"(", 
         RowBox[{"k2", "+", "k3", "+", "r2"}], ")"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"r1", "+", "r2"}], ")"}], " ", "r3"}], "+", 
       RowBox[{"k2", " ", 
        RowBox[{"(", 
         RowBox[{"k3", "+", "r3"}], ")"}]}]}]]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{
  3.754409976383402*^9, 3.7571712673519435`*^9, 3.7571730638906927`*^9, 
   3.7578875825609393`*^9, 3.7578879920047026`*^9, 3.757944928027196*^9, 
   3.7603864920371757`*^9, 3.760386527729007*^9, 3.7603890369632797`*^9, {
   3.7605449253934956`*^9, 3.7605449408781085`*^9}},
 CellLabel->"Out[3]=",ExpressionUUID->"be11f586-04e4-48ca-9a5e-43aac7271575"]
}, Open  ]],

Cell[TextData[StyleBox["Our specific system has symmetry, concentration \
dependence of activator binding (c) and energy input (b) on edge from state \
2->1. when b = 1 then the system obeys detailed balance by construction", \
"Subsubsection"]], "Text",
 CellChangeTimes->{{3.750384069847657*^9, 3.750384073926244*^9}, {
  3.75413811648597*^9, 3.754138144004209*^9}, {3.754138220034409*^9, 
  3.754138238383642*^9}, {3.754139444317975*^9, 3.754139471025515*^9}, {
  3.754409984809718*^9, 
  3.754409985084999*^9}},ExpressionUUID->"57476a6a-3be2-4c96-bdb7-\
d81fff26d895"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"symSol", " ", "=", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{"sol", "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"k1", "\[Rule]", "  ", "a"}], ",", " ", 
      RowBox[{"r1", "\[Rule]", "  ", 
       RowBox[{"c", " ", "koff"}]}], ",", "  ", 
      RowBox[{"k2", "\[Rule]", " ", 
       RowBox[{"b", " ", "kp"}]}], ",", " ", 
      RowBox[{"r2", "\[Rule]", " ", "km"}], ",", " ", 
      RowBox[{"k3", "\[Rule]", 
       RowBox[{"c", " ", "koff"}]}], ",", " ", 
      RowBox[{"r3", "\[Rule]", " ", 
       RowBox[{"a", " ", "*", " ", 
        RowBox[{"kp", " ", "/", " ", "km"}]}]}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.750270828442218*^9, 3.7502709253892717`*^9}, {
   3.750271135055722*^9, 3.7502711415787287`*^9}, {3.750273120141753*^9, 
   3.7502731252525797`*^9}, 3.750273174328928*^9, {3.75034470917811*^9, 
   3.7503447093443947`*^9}, {3.750357805818123*^9, 3.750357835662365*^9}, {
   3.75413825427017*^9, 3.754138272719884*^9}, {3.7579414376414356`*^9, 
   3.757941458583452*^9}, {3.757944579615947*^9, 3.75794458947511*^9}, 
   3.7579448476686354`*^9, {3.757944884629066*^9, 3.7579448853567142`*^9}, {
   3.760386413094081*^9, 3.7603864503105707`*^9}, {3.760389013620601*^9, 
   3.760389030634039*^9}, 3.7603890976627707`*^9, {3.7603891619719553`*^9, 
   3.7603891660648317`*^9}, {3.7605450097111373`*^9, 
   3.7605450825691123`*^9}, {3.760545164586995*^9, 3.7605451730012712`*^9}, 
   3.760545270525172*^9, {3.7605453191253777`*^9, 3.7605453192161365`*^9}, 
   3.760545385121646*^9, {3.7605455177114697`*^9, 3.7605455267854395`*^9}},
 CellLabel->"In[31]:=",ExpressionUUID->"4d1ce54a-26aa-4a0e-866a-5330bf9dd464"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"J", "\[Rule]", 
     FractionBox[
      RowBox[{"a", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", "b"}], ")"}], " ", "c", " ", "km", " ", 
       "koff", " ", "kp"}], 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"km", "+", 
         RowBox[{"c", " ", "koff"}], "+", 
         RowBox[{"b", " ", "kp"}]}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"c", " ", "km", " ", "koff"}], "+", 
         RowBox[{"a", " ", 
          RowBox[{"(", 
           RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}]]}], ",", 
    RowBox[{"p1", "\[Rule]", 
     FractionBox[
      RowBox[{"c", " ", "km", " ", "koff"}], 
      RowBox[{
       RowBox[{"c", " ", "km", " ", "koff"}], "+", 
       RowBox[{"a", " ", 
        RowBox[{"(", 
         RowBox[{"km", "+", "kp"}], ")"}]}]}]]}], ",", 
    RowBox[{"p2", "\[Rule]", 
     FractionBox[
      RowBox[{"a", " ", "km", " ", 
       RowBox[{"(", 
        RowBox[{"km", "+", 
         RowBox[{"c", " ", "koff"}], "+", "kp"}], ")"}]}], 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"km", "+", 
         RowBox[{"c", " ", "koff"}], "+", 
         RowBox[{"b", " ", "kp"}]}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"c", " ", "km", " ", "koff"}], "+", 
         RowBox[{"a", " ", 
          RowBox[{"(", 
           RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}]]}], ",", 
    RowBox[{"p3", "\[Rule]", 
     FractionBox[
      RowBox[{"a", " ", "kp", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"c", " ", "koff"}], "+", 
         RowBox[{"b", " ", 
          RowBox[{"(", 
           RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}], 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"km", "+", 
         RowBox[{"c", " ", "koff"}], "+", 
         RowBox[{"b", " ", "kp"}]}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"c", " ", "km", " ", "koff"}], "+", 
         RowBox[{"a", " ", 
          RowBox[{"(", 
           RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}]]}]}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.750270919819333*^9, 3.750270925973634*^9}, 
   3.750271142598151*^9, 3.750273125986121*^9, 3.750273175023505*^9, 
   3.750343772690199*^9, 3.75034471093965*^9, 3.75035780658092*^9, 
   3.75035783684374*^9, {3.7541382417816057`*^9, 3.754138273639433*^9}, 
   3.754142623945938*^9, 3.7541434792438393`*^9, 3.7541442793190804`*^9, 
   3.7541445868881187`*^9, 3.754409987836501*^9, 3.7571712678196926`*^9, 
   3.7571730643025894`*^9, 3.7578875829275*^9, 3.757887992033626*^9, 
   3.757941462243665*^9, 3.7579445928247776`*^9, 3.7579448875843215`*^9, 
   3.757944928354303*^9, 3.760386492857601*^9, 3.7603865281031785`*^9, 
   3.7603890372149453`*^9, {3.760545074953227*^9, 3.7605450833929415`*^9}, 
   3.760545174303788*^9, 3.7605452711116056`*^9, 3.760545319996051*^9, 
   3.760545385823768*^9, 3.760545532522269*^9},
 CellLabel->"Out[31]=",ExpressionUUID->"bf883259-2e5a-4f3c-aa5f-1b3a84d27623"]
}, Open  ]],

Cell[TextData[StyleBox["We are treating state 3 as the productive state - \
i.e. the one in which transcription happens. Copied p3 from above solution", \
"Subsubsection"]], "Text",
 CellChangeTimes->{{3.754138276459917*^9, 3.754138333055722*^9}, {
  3.754138513073224*^9, 3.754138513839487*^9}, {3.754138654558078*^9, 
  3.754138728796371*^9}, {3.754409995772583*^9, 
  3.754410002669238*^9}},ExpressionUUID->"19646b47-4fc7-459f-8a88-\
4f006c354ecf"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"p3cb", "[", "c_", "]"}], " ", "=", " ", 
   RowBox[{"p3", "/.", " ", 
    RowBox[{"symSol", "[", 
     RowBox[{"[", "1", "]"}], "]"}]}]}], "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.750270828442218*^9, 3.7502709253892717`*^9}, {
   3.750271135055722*^9, 3.7502711415787287`*^9}, {3.750273120141753*^9, 
   3.7502731252525797`*^9}, 3.750273174328928*^9, {3.75034470917811*^9, 
   3.7503447093443947`*^9}, {3.750357805818123*^9, 3.750357835662365*^9}, {
   3.75413825427017*^9, 3.754138272719884*^9}, {3.75413874371506*^9, 
   3.754138837305544*^9}, {3.754142263007573*^9, 3.754142265582119*^9}, 
   3.7579414943068676`*^9, 3.7579415653539877`*^9, 3.7579446227600155`*^9, 
   3.757944903143441*^9, 3.760386523391152*^9},
 CellLabel->"In[32]:=",ExpressionUUID->"5335b349-278a-4278-9db8-36a1606a1160"],

Cell[BoxData[
 FractionBox[
  RowBox[{"a", " ", "kp", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c", " ", "koff"}], "+", 
     RowBox[{"b", " ", 
      RowBox[{"(", 
       RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}], 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"km", "+", 
     RowBox[{"c", " ", "koff"}], "+", 
     RowBox[{"b", " ", "kp"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c", " ", "km", " ", "koff"}], "+", 
     RowBox[{"a", " ", 
      RowBox[{"(", 
       RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}]]], "Output",
 CellChangeTimes->{{3.7541388082074537`*^9, 3.754138840663299*^9}, 
   3.7541426240271187`*^9, 3.754143479343245*^9, 3.754144279399077*^9, 
   3.754144599363389*^9, 3.754410005935557*^9, 3.757171267909452*^9, 
   3.7571730644432144`*^9, 3.7578879921343565`*^9, {3.757941476694544*^9, 
   3.7579414967054534`*^9}, {3.757941543588135*^9, 3.7579415732289333`*^9}, {
   3.7579446291640377`*^9, 3.757944649668927*^9}, {3.757944904248955*^9, 
   3.757944928398018*^9}, 3.760386492938695*^9, 3.7603865281335382`*^9, 
   3.760389037247357*^9, 3.760545090127906*^9, 3.760545178042806*^9, 
   3.760545273766508*^9, 3.7605453236702337`*^9, 3.7605453896035247`*^9, 
   3.7605455349940243`*^9},
 CellLabel->"Out[32]=",ExpressionUUID->"a18a61b7-05ab-44a4-bc48-8cae8c8c8091"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"p3c", "[", "c_", "]"}], " ", "=", " ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"p3cb", "[", "c", "]"}], " ", "/.", " ", 
    RowBox[{"{", 
     RowBox[{"b", " ", "\[Rule]", " ", "1"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7579418506015167`*^9, 3.7579418532893286`*^9}},
 FontWeight->"Plain",
 CellLabel->"In[33]:=",ExpressionUUID->"29caa98b-827d-4d0f-bbc5-3529f63893b5"],

Cell[BoxData[
 FractionBox[
  RowBox[{"a", " ", "kp"}], 
  RowBox[{
   RowBox[{"c", " ", "km", " ", "koff"}], "+", 
   RowBox[{"a", " ", 
    RowBox[{"(", 
     RowBox[{"km", "+", "kp"}], ")"}]}]}]]], "Output",
 CellChangeTimes->{{3.7541388082074537`*^9, 3.754138840663299*^9}, 
   3.7541426240271187`*^9, 3.754143479343245*^9, 3.754144279399077*^9, 
   3.754144599363389*^9, 3.754410005935557*^9, 3.757171267909452*^9, 
   3.7571730644432144`*^9, 3.7578879921343565`*^9, {3.757941476694544*^9, 
   3.7579414967054534`*^9}, {3.757941543588135*^9, 3.7579415692455826`*^9}, 
   3.757941854669639*^9, 3.757944632669814*^9, {3.7579449076081743`*^9, 
   3.757944928438404*^9}, 3.760386492976909*^9, 3.760386528295678*^9, 
   3.7603890372777195`*^9, 3.7605451023144064`*^9, 3.760545182338376*^9, 
   3.760545277892043*^9, 3.7605453278695173`*^9, 3.760545392965663*^9, 
   3.7605455374447603`*^9},
 CellLabel->"Out[33]=",ExpressionUUID->"5f517600-14e4-4784-acb1-26f03c9d8dc7"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Calculate fidelity ratios in/out of equilibrium", "Subsubsection",
 CellChangeTimes->{
  3.7578882507802095`*^9},ExpressionUUID->"87de3d82-d3a6-4441-a929-\
c6412d28cd6e"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"eqRatio", " ", "=", " ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"p3c", "[", "c0", "]"}], " ", "/", " ", 
    RowBox[{"p3c", "[", "c1", "]"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"noneqRatio", " ", "=", " ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"p3cb", "[", "c0", "]"}], " ", "/", " ", 
    RowBox[{"p3cb", "[", "c1", "]"}]}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.754142610648045*^9, 3.7541426633604*^9}, {
   3.754143075787678*^9, 3.7541430849103813`*^9}, {3.7544097702047853`*^9, 
   3.7544097708120832`*^9}, {3.754410035553958*^9, 3.754410046050346*^9}, {
   3.7578874150656567`*^9, 3.7578874156121974`*^9}, {3.757941657732047*^9, 
   3.7579416647548103`*^9}, 3.760387435301339*^9},
 CellLabel->"In[34]:=",ExpressionUUID->"a3260b4f-c2f6-4d0a-b124-5046d4161368"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"c1", " ", "km", " ", "koff"}], "+", 
   RowBox[{"a", " ", 
    RowBox[{"(", 
     RowBox[{"km", "+", "kp"}], ")"}]}]}], 
  RowBox[{
   RowBox[{"c0", " ", "km", " ", "koff"}], "+", 
   RowBox[{"a", " ", 
    RowBox[{"(", 
     RowBox[{"km", "+", "kp"}], ")"}]}]}]]], "Output",
 CellChangeTimes->{3.7605453379076962`*^9, 3.7605453958569355`*^9, 
  3.7605455474696355`*^9},
 CellLabel->"Out[34]=",ExpressionUUID->"02aa9c0f-8fac-455f-936e-926ae0aef3d3"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"(", 
    RowBox[{"km", "+", 
     RowBox[{"c1", " ", "koff"}], "+", 
     RowBox[{"b", " ", "kp"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c1", " ", "km", " ", "koff"}], "+", 
     RowBox[{"a", " ", 
      RowBox[{"(", 
       RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c0", " ", "koff"}], "+", 
     RowBox[{"b", " ", 
      RowBox[{"(", 
       RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}], 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"km", "+", 
     RowBox[{"c0", " ", "koff"}], "+", 
     RowBox[{"b", " ", "kp"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c0", " ", "km", " ", "koff"}], "+", 
     RowBox[{"a", " ", 
      RowBox[{"(", 
       RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c1", " ", "koff"}], "+", 
     RowBox[{"b", " ", 
      RowBox[{"(", 
       RowBox[{"km", "+", "kp"}], ")"}]}]}], ")"}]}]]], "Output",
 CellChangeTimes->{3.7605453379076962`*^9, 3.7605453958569355`*^9, 
  3.760545547559345*^9},
 CellLabel->"Out[35]=",ExpressionUUID->"e4462d4f-9125-473d-b722-a81ba8926758"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"noneq", " ", "=", " ", 
   RowBox[{
    RowBox[{
    "eq", " ", "*", " ", "gamma", " ", "*", " ", "delta", " ", 
     "\[IndentingNewLine]", "noneqComponent"}], " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"noneqRatio", " ", "/", " ", "eqRatio"}], "\[IndentingNewLine]",
       "gamma"}], " ", "=", " ", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"b", " ", "km"}], "+", "koff", "+", 
          RowBox[{"c1", " ", "kon"}], "+", "kp"}], ")"}], " ", "/", " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"b", " ", "km"}], "+", "koff", "+", 
          RowBox[{"c0", " ", "kon"}], "+", "kp"}], ")"}]}], 
       "\[IndentingNewLine]", "delta"}], " ", "=", " ", 
      RowBox[{"noneqComponent", " ", "/", " ", "gamma"}]}]}]}]}], 
  "*)"}]], "Input",
 CellChangeTimes->{{3.754142694192856*^9, 3.754142742382628*^9}, {
   3.75414277866681*^9, 3.754142780308208*^9}, {3.754142964950635*^9, 
   3.754143024833893*^9}, {3.7541430707929564`*^9, 3.754143193723753*^9}, {
   3.754143241707274*^9, 3.754143242067079*^9}, {3.754143579018424*^9, 
   3.754143581060046*^9}, 3.754144489118615*^9, {3.7541560167451773`*^9, 
   3.754156082212016*^9}, {3.754156297150043*^9, 3.7541563386161547`*^9}, {
   3.75415642162276*^9, 3.754156422679317*^9}, 3.754158628148871*^9, {
   3.754159446226882*^9, 3.754159449589402*^9}, {3.754159649847518*^9, 
   3.7541596876352386`*^9}, {3.754159762249597*^9, 3.754159764621025*^9}, {
   3.754160347389367*^9, 3.754160432992386*^9}, {3.7541605020930347`*^9, 
   3.754160502542756*^9}, {3.754160623182036*^9, 3.754160625496665*^9}, {
   3.754166238064845*^9, 3.754166319786064*^9}, {3.754166458831888*^9, 
   3.754166470754583*^9}, {3.754409736666646*^9, 3.754409784311348*^9}, {
   3.757887966409135*^9, 3.7578879800935473`*^9}},
 CellLabel->
  "In[725]:=",ExpressionUUID->"3319b6c7-476b-4a90-9616-4538caac68d1"],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.757171183744201*^9, 3.757171185119487*^9}, 
   3.757887969567689*^9},
 CellLabel->
  "In[726]:=",ExpressionUUID->"ffa10b85-0cde-43e2-ac8b-9d49f8037980"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Solve for optimality conditions", "Subsection",
 CellChangeTimes->{{3.7579422136491475`*^9, 3.757942230143794*^9}, {
  3.7603865714528155`*^9, 3.760386579755602*^9}, {3.7603867297468834`*^9, 
  3.760386762235937*^9}, {3.760546046310901*^9, 
  3.760546068899332*^9}},ExpressionUUID->"7aaf2638-a90b-4dde-b4f6-\
8769f7ba83fa"],

Cell[CellGroupData[{

Cell["Render things in units of kp", "Subsubsection",
 CellChangeTimes->{{3.7605460908777375`*^9, 
  3.760546097361009*^9}},ExpressionUUID->"0d137d1e-f9d1-4851-bae9-\
c104cb3ca288"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"noneqRatioSimp", " ", "=", " ", 
  RowBox[{"N", "[", " ", 
   RowBox[{"noneqRatio", " ", "/.", " ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"kp", "\[Rule]", "1"}], ",", 
      RowBox[{"a", "\[Rule]", "10"}], ",", 
      RowBox[{"km", "\[Rule]", "1000"}], ",", 
      RowBox[{"c0", "\[Rule]", "1"}], ",", 
      RowBox[{"c1", "\[Rule]", "10"}], ",", 
      RowBox[{"koff", "\[Rule]", "10000"}], ",", 
      RowBox[{"b", "\[Rule]", "50000"}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7579422436157813`*^9, 3.757942270170826*^9}, {
   3.757944721844734*^9, 3.7579447591640224`*^9}, 3.757944977798463*^9, {
   3.757945107515103*^9, 3.757945110411311*^9}, 3.7603890794122105`*^9, {
   3.7605456073612165`*^9, 3.7605456077642746`*^9}, {3.760546107943945*^9, 
   3.760546125759443*^9}, {3.7605466062951264`*^9, 3.760546634348949*^9}, {
   3.7605467034124794`*^9, 3.7605467862171364`*^9}, {3.760546887204542*^9, 
   3.7605468933267384`*^9}, {3.760546985382968*^9, 3.7605470145434475`*^9}},
 CellLabel->"In[59]:=",ExpressionUUID->"53dd90b0-e7fb-4a9d-b655-38acb1de1947"],

Cell[BoxData["24.687435571306903`"], "Output",
 CellChangeTimes->{
  3.757942271778528*^9, 3.7579446634935007`*^9, 3.7579447519915686`*^9, {
   3.7579449723690586`*^9, 3.757944980086499*^9}, 3.757945112136796*^9, 
   3.760386493597438*^9, 3.7603865298891993`*^9, 3.7603890380559483`*^9, 
   3.760545611724201*^9, 3.760546126569666*^9, {3.7605466194111447`*^9, 
   3.760546635595546*^9}, {3.760546708588098*^9, 3.7605467867962036`*^9}, {
   3.76054688821637*^9, 3.7605468943404503`*^9}, {3.760546989675107*^9, 
   3.7605470153972025`*^9}},
 CellLabel->"Out[59]=",ExpressionUUID->"6e3050d1-aa6f-488a-8a57-6d346ccd6f3c"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Maximize with respect to b", "Subsubsection",
 CellChangeTimes->{{3.7605462578505726`*^9, 
  3.7605462644635115`*^9}},ExpressionUUID->"7aad0384-6493-46f8-9ab6-\
da1134f0810f"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Maximize", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"noneqRatioSimp", "&&", " ", 
     RowBox[{"b", " ", ">", " ", "0"}]}], "}"}], ",", "b"}], "]"}]], "Input",
 CellChangeTimes->{{3.760546310415678*^9, 3.7605463256850777`*^9}, {
  3.760546896768446*^9, 3.760546924080724*^9}},
 CellLabel->"In[56]:=",ExpressionUUID->"fa0615b3-2904-4315-a345-92afa9705138"],

Cell[BoxData[
 RowBox[{"Maximize", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     FractionBox[
      RowBox[{"3333667", " ", 
       RowBox[{"(", 
        RowBox[{"101000", "+", "b"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{"10000", "+", 
         RowBox[{"1001", " ", "b"}]}], ")"}]}], 
      RowBox[{"333667", " ", 
       RowBox[{"(", 
        RowBox[{"11000", "+", "b"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{"100000", "+", 
         RowBox[{"1001", " ", "b"}]}], ")"}]}]], "&&", 
     RowBox[{"b", ">", "0"}]}], "}"}], ",", "b"}], "]"}]], "Output",
 CellChangeTimes->{
  3.760546501671181*^9, 3.760546644182288*^9, 3.760546713047062*^9, {
   3.760546900836978*^9, 3.7605469245156717`*^9}},
 CellLabel->"Out[56]=",ExpressionUUID->"b6fccc86-cdec-4cc8-92e6-01dae5add3ce"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1920, 1037},
WindowMargins->{{1912, Automatic}, {Automatic, -8}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 356, 5, 40, "Text",ExpressionUUID->"d1518c95-f8f3-413c-b038-17f90c0a1e60"],
Cell[917, 27, 889, 18, 28, "Input",ExpressionUUID->"c95437b1-8467-44ee-b3ad-5728a54b961b"],
Cell[CellGroupData[{
Cell[1831, 49, 1272, 29, 28, "Input",ExpressionUUID->"7fb5bbc1-0e7f-4bb4-a355-efe1fde738a9"],
Cell[3106, 80, 2935, 87, 59, "Output",ExpressionUUID->"be11f586-04e4-48ca-9a5e-43aac7271575"]
}, Open  ]],
Cell[6056, 170, 572, 9, 39, "Text",ExpressionUUID->"57476a6a-3be2-4c96-bdb7-d81fff26d895"],
Cell[CellGroupData[{
Cell[6653, 183, 1670, 30, 28, "Input",ExpressionUUID->"4d1ce54a-26aa-4a0e-866a-5330bf9dd464"],
Cell[8326, 215, 3048, 80, 59, "Output",ExpressionUUID->"bf883259-2e5a-4f3c-aa5f-1b3a84d27623"]
}, Open  ]],
Cell[11389, 298, 451, 7, 39, "Text",ExpressionUUID->"19646b47-4fc7-459f-8a88-4f006c354ecf"],
Cell[CellGroupData[{
Cell[11865, 309, 858, 15, 48, "Input",ExpressionUUID->"5335b349-278a-4278-9db8-36a1606a1160"],
Cell[12726, 326, 1315, 30, 59, "Output",ExpressionUUID->"a18a61b7-05ab-44a4-bc48-8cae8c8c8091"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14078, 361, 435, 10, 28, "Input",ExpressionUUID->"29caa98b-827d-4d0f-bbc5-3529f63893b5"],
Cell[14516, 373, 969, 18, 59, "Output",ExpressionUUID->"5f517600-14e4-4784-acb1-26f03c9d8dc7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15522, 396, 176, 3, 44, "Subsubsection",ExpressionUUID->"87de3d82-d3a6-4441-a929-c6412d28cd6e"],
Cell[CellGroupData[{
Cell[15723, 403, 839, 16, 48, "Input",ExpressionUUID->"a3260b4f-c2f6-4d0a-b124-5046d4161368"],
Cell[16565, 421, 500, 14, 59, "Output",ExpressionUUID->"02aa9c0f-8fac-455f-936e-926ae0aef3d3"],
Cell[17068, 437, 1190, 38, 59, "Output",ExpressionUUID->"e4462d4f-9125-473d-b722-a81ba8926758"]
}, Open  ]],
Cell[18273, 478, 1959, 40, 86, "Input",ExpressionUUID->"3319b6c7-476b-4a90-9616-4538caac68d1"],
Cell[20235, 520, 201, 4, 28, "Input",ExpressionUUID->"ffa10b85-0cde-43e2-ac8b-9d49f8037980"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20473, 529, 329, 5, 53, "Subsection",ExpressionUUID->"7aaf2638-a90b-4dde-b4f6-8769f7ba83fa"],
Cell[CellGroupData[{
Cell[20827, 538, 181, 3, 44, "Subsubsection",ExpressionUUID->"0d137d1e-f9d1-4851-bae9-c104cb3ca288"],
Cell[CellGroupData[{
Cell[21033, 545, 1102, 20, 28, "Input",ExpressionUUID->"53dd90b0-e7fb-4a9d-b655-38acb1de1947"],
Cell[22138, 567, 617, 9, 32, "Output",ExpressionUUID->"6e3050d1-aa6f-488a-8a57-6d346ccd6f3c"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[22804, 582, 181, 3, 44, "Subsubsection",ExpressionUUID->"7aad0384-6493-46f8-9ab6-da1134f0810f"],
Cell[CellGroupData[{
Cell[23010, 589, 384, 8, 28, "Input",ExpressionUUID->"fa0615b3-2904-4315-a345-92afa9705138"],
Cell[23397, 599, 802, 22, 59, "Output",ExpressionUUID->"b6fccc86-cdec-4cc8-92e6-01dae5add3ce"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

