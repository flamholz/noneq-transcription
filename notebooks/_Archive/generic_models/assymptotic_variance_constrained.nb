(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     27486,        765]
NotebookOptionsPosition[     23398,        697]
NotebookOutlinePosition[     23741,        712]
CellTagsIndexPosition[     23698,        709]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
Verifying Expressions for Expectation and Variation of Accumulated Reward \
from Markov Reward Process\
\>", "Section",
 CellChangeTimes->{{3.766585934056647*^9, 
  3.766585974405374*^9}},ExpressionUUID->"0cd71903-4765-4253-86c7-\
583733eb7283"],

Cell[BoxData[
 RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.7665860727694225`*^9, 3.7665860727953434`*^9}},
 CellLabel->"In[82]:=",ExpressionUUID->"f53ddaf6-0803-4542-a92f-8240a1d82c27"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ass", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"c", ">", "0"}], ",", " ", 
     RowBox[{"kp", ">", "0"}], ",", " ", 
     RowBox[{"km", ">", "0"}], ",", " ", 
     RowBox[{"koff", ">", "0"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.773151531769226*^9, 3.773151564743101*^9}},
 CellLabel->"In[83]:=",ExpressionUUID->"7d9e31ed-9dd1-483b-a957-1f1d4020b646"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Q4", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "kp"}], "-", "c"}], ",", "     ", "koff", ",", 
       "                  ", "0", ",", "          ", "1"}], " ", "}"}], ",", 
     "\[IndentingNewLine]", "         ", 
     RowBox[{"{", 
      RowBox[{"c", ",", "         ", 
       RowBox[{
        RowBox[{"-", "koff"}], "-", "kp"}], ",", "            ", "1", ",", 
       "          ", "0"}], "}"}], ",", "\[IndentingNewLine]", "         ", 
     RowBox[{"{", 
      RowBox[{"0", ",", "                   ", "kp", ",", "       ", 
       RowBox[{
        RowBox[{"-", "koff"}], "-", "1"}], ",", "     ", "c"}], "}"}], ",", 
     "\[IndentingNewLine]", "         ", 
     RowBox[{"{", 
      RowBox[{
      "kp", ",", "                  ", "0", ",", "                  ", "koff",
        ",", "  ", 
       RowBox[{
        RowBox[{"-", "c"}], "-", "1"}]}], "}"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7665970485156336`*^9, 3.7665970506267967`*^9}, {
   3.76659709635977*^9, 3.7665972549757605`*^9}, 3.766597670769878*^9, {
   3.76659877422307*^9, 3.766598798872613*^9}, 3.7668379732594795`*^9, {
   3.7668380192182255`*^9, 3.7668380333733864`*^9}, 3.7673043615049086`*^9, {
   3.767304474930873*^9, 3.7673044962673674`*^9}, {3.7731512671132526`*^9, 
   3.7731512816774855`*^9}, {3.773151323238453*^9, 3.7731514797083254`*^9}, 
   3.773151512774988*^9, {3.7731523994341583`*^9, 3.773152408328412*^9}},
 CellLabel->"In[84]:=",ExpressionUUID->"9c208344-b914-4a91-af3b-57aa27d1e452"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", "Q4", "]"}]], "Input",
 NumberMarks->False,
 CellLabel->"In[85]:=",ExpressionUUID->"c1da6231-a226-4878-886a-1ae8cbefdc95"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       RowBox[{"-", "c"}], "-", "kp"}], "koff", "0", "1"},
     {"c", 
      RowBox[{
       RowBox[{"-", "koff"}], "-", "kp"}], "1", "0"},
     {"0", "kp", 
      RowBox[{
       RowBox[{"-", "1"}], "-", "koff"}], "c"},
     {"kp", "0", "koff", 
      RowBox[{
       RowBox[{"-", "1"}], "-", "c"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.773151496629105*^9, 3.773152412029496*^9},
 CellLabel->
  "Out[85]//MatrixForm=",ExpressionUUID->"072ec8d5-a714-41df-b696-\
0637eb85c3c6"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"Refine", "[", 
   RowBox[{
    RowBox[{"Eigenvalues", "[", "Q4", "]"}], ",", 
    RowBox[{"Assumptions", "\[Rule]", " ", "ass"}]}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.7665978637781224`*^9, 3.766597906076456*^9}, 
   3.7665980593033695`*^9},
 CellLabel->"In[86]:=",ExpressionUUID->"59cb4cf4-27f2-4409-937f-21b6551d42d3"],

Cell[BoxData[
 RowBox[{
  RowBox[{"eigVectors", " ", "=", " ", 
   RowBox[{"Eigenvectors", "[", "Q4", "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7665979196138344`*^9, 3.7665979301282406`*^9}},
 CellLabel->"In[87]:=",ExpressionUUID->"5d25927e-1ef7-47f6-a1dc-8569ff8b995e"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ssRaw", " ", "=", " ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"eigVectors", "[", 
     RowBox[{"[", "1", "]"}], "]"}], " ", "/", " ", 
    RowBox[{"Total", "[", 
     RowBox[{"eigVectors", "[", 
      RowBox[{"[", "1", "]"}], "]"}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.766597934072912*^9, 3.7665979843979015`*^9}, {
   3.766598018209607*^9, 3.7665980277008443`*^9}, 3.773151589061305*^9},
 CellLabel->"In[88]:=",ExpressionUUID->"1b51bc59-d5a1-412b-9f72-d20ee10c2b54"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["koff", 
    RowBox[{"c", "+", "koff", "+", 
     RowBox[{"c", " ", "kp"}], "+", 
     RowBox[{"koff", " ", "kp"}]}]], ",", 
   FractionBox["c", 
    RowBox[{"c", "+", "koff", "+", 
     RowBox[{"c", " ", "kp"}], "+", 
     RowBox[{"koff", " ", "kp"}]}]], ",", 
   FractionBox[
    RowBox[{"c", " ", "kp"}], 
    RowBox[{"c", "+", "koff", "+", 
     RowBox[{"c", " ", "kp"}], "+", 
     RowBox[{"koff", " ", "kp"}]}]], ",", 
   FractionBox[
    RowBox[{"koff", " ", "kp"}], 
    RowBox[{"c", "+", "koff", "+", 
     RowBox[{"c", " ", "kp"}], "+", 
     RowBox[{"koff", " ", "kp"}]}]]}], "}"}]], "Output",
 CellChangeTimes->{3.77315158967769*^9, 3.773152412230959*^9},
 CellLabel->"Out[88]=",ExpressionUUID->"0d16e703-a497-4eb9-aa6e-b398f31a8cae"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7665980880250993`*^9, 3.7665981617711353`*^9}, {
   3.766599697241766*^9, 3.766599698650004*^9}, 3.766601218251429*^9},
 CellLabel->"In[89]:=",ExpressionUUID->"4fe61074-1312-425c-ae10-f001d7295dbd"],

Cell[BoxData[
 RowBox[{
  RowBox[{"r", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "1", ",", "0"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766599276299369*^9, 3.76659928364874*^9}},
 CellLabel->"In[90]:=",ExpressionUUID->"fd7c28c3-652c-4b27-b207-09b80f324c5f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"g4", " ", "=", " ", 
   RowBox[{"r", ".", "ssRaw"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766599915617947*^9, 3.7665999286536217`*^9}},
 CellLabel->"In[91]:=",ExpressionUUID->"dd6edc2a-abb3-4355-b2b7-a9b600e99bc1"],

Cell[CellGroupData[{

Cell["Define system of equations to find w\[CloseCurlyQuote]s", \
"Subsubsection",
 CellChangeTimes->{{3.7665992529379115`*^9, 
  3.7665992651413913`*^9}},ExpressionUUID->"4bbb22a6-f9b4-4c69-8d47-\
32491673556f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"wVecRaw", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{"w1", ",", "w2", ",", "w3", ",", "w4"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766599966577458*^9, 3.766599970158909*^9}, {
  3.766601280481094*^9, 3.7666012816579475`*^9}},
 CellLabel->"In[92]:=",ExpressionUUID->"ca8e9be0-0b84-4e9d-9416-0b70501605ff"],

Cell[BoxData[
 RowBox[{
  RowBox[{"eq1", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"r", "[", 
      RowBox[{"[", "1", "]"}], "]"}], " ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"2", ",", "1"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w2", "-", "w1"}], ")"}]}], "  ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"4", ",", "1"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w4", "-", "w1"}], ")"}]}]}], " ", "\[Equal]", " ", "g4"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.766599268941347*^9, 3.7665994312433834`*^9}, {
  3.7665995074780436`*^9, 3.766599508997341*^9}},
 CellLabel->"In[93]:=",ExpressionUUID->"627ec138-4cf3-45df-95dd-9dca55ad5796"],

Cell[BoxData[
 RowBox[{
  RowBox[{"eq2", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"r", "[", 
      RowBox[{"[", "2", "]"}], "]"}], " ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"1", ",", "2"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w1", "-", "w2"}], ")"}]}], "  ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"3", ",", "2"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w3", "-", "w2"}], ")"}]}]}], " ", "\[Equal]", " ", "g4"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.766599393798574*^9, 3.766599393874333*^9}, {
  3.7665995132929764`*^9, 3.7665995436005173`*^9}},
 CellLabel->"In[94]:=",ExpressionUUID->"f498e0d9-a95c-4e8d-bc15-854e2613ed30"],

Cell[BoxData[
 RowBox[{
  RowBox[{"eq3", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"r", "[", 
      RowBox[{"[", "3", "]"}], "]"}], " ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"2", ",", "3"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w2", "-", "w3"}], ")"}]}], "  ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"4", ",", "3"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w4", "-", "w3"}], ")"}]}]}], " ", "\[Equal]", " ", "g4"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.766599550238562*^9, 3.7665995774927416`*^9}},
 CellLabel->"In[95]:=",ExpressionUUID->"bf44010f-bcbe-4e20-b538-313668dbb678"],

Cell[BoxData[
 RowBox[{
  RowBox[{"eq4", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"r", "[", 
      RowBox[{"[", "4", "]"}], "]"}], " ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"1", ",", "4"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w1", "-", "w4"}], ")"}]}], "  ", "+", " ", 
     RowBox[{
      RowBox[{"Q4", "[", 
       RowBox[{"[", 
        RowBox[{"3", ",", "4"}], "]"}], "]"}], "*", 
      RowBox[{"(", 
       RowBox[{"w3", "-", "w4"}], ")"}]}]}], " ", "\[Equal]", " ", "g4"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.766599585241108*^9, 3.7665996301698136`*^9}},
 CellLabel->"In[96]:=",ExpressionUUID->"fa518dc3-7d43-4e49-af75-3f71a5204214"],

Cell[BoxData[
 RowBox[{
  RowBox[{"eq5", "=", " ", 
   RowBox[{
    RowBox[{"ssRaw", ".", "wVecRaw"}], "\[Equal]", "0"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766599455613988*^9, 3.7665995036213503`*^9}, {
   3.7665996646438036`*^9, 3.7665997433036566`*^9}, {3.7665998437946806`*^9, 
   3.766599872805852*^9}, 3.766599962605076*^9, {3.7666012519164586`*^9, 
   3.7666012534184294`*^9}, {3.7666012855535345`*^9, 3.7666012857769704`*^9}},
 CellLabel->"In[97]:=",ExpressionUUID->"e379dcb2-a5ad-40f2-9be5-267b2ed7185a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"solw", " ", "=", " ", 
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"eq1", ",", "eq2", ",", "eq3", ",", "eq4", ",", "eq5"}], "}"}], 
     ",", 
     RowBox[{"{", 
      RowBox[{"w1", ",", "w2", ",", "w3", ",", "w4"}], "}"}]}], "]"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.766599877817956*^9, 3.7665999526670647`*^9}},
 CellLabel->"In[98]:=",ExpressionUUID->"2c6f4674-9887-4b04-8adc-f8feb90a44c3"],

Cell[BoxData[
 RowBox[{
  RowBox[{"wVec", " ", "=", " ", 
   RowBox[{"ArrayReshape", "[", 
    RowBox[{
     RowBox[{"wVecRaw", " ", "/.", "solw"}], ",", "4"}], "]"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.7665999770300136`*^9, 3.766600042517169*^9}, {
  3.7666012650912247`*^9, 3.766601290929227*^9}, {3.7666018532581973`*^9, 
  3.7666018668055086`*^9}},
 CellLabel->"In[99]:=",ExpressionUUID->"77bcbbd5-e42c-4088-8b3e-43edf1ecbb7a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"s1", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"2", ",", "1"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "2", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "1", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"4", ",", "1"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "4", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "1", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{"2", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"r", "[", 
        RowBox[{"[", "1", "]"}], "]"}], "-", "g4"}], ")"}], "*", 
     RowBox[{"wVec", "[", 
      RowBox[{"[", "1", "]"}], "]"}]}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7666000339891157`*^9, 3.7666000526993103`*^9}, {
   3.766600168811973*^9, 3.766600259767084*^9}, {3.7666002976929703`*^9, 
   3.7666003037956977`*^9}, 3.766600361377178*^9, {3.7666014091896005`*^9, 
   3.7666014132432065`*^9}, 3.7666016212978563`*^9, {3.766601662830435*^9, 
   3.7666016900218353`*^9}, 3.7666019135371017`*^9},
 CellLabel->
  "In[100]:=",ExpressionUUID->"0e464976-b146-49e2-9878-98e36c496bc6"],

Cell[BoxData[
 RowBox[{
  RowBox[{"s2", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"1", ",", "2"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "1", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "2", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"3", ",", "2"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "3", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "2", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{"2", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"r", "[", 
        RowBox[{"[", "2", "]"}], "]"}], "-", "g4"}], ")"}], "*", 
     RowBox[{"wVec", "[", 
      RowBox[{"[", "2", "]"}], "]"}]}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766600268862155*^9, 3.766600285848935*^9}, {
  3.766600369384775*^9, 3.7666004064354115`*^9}, {3.766600481166391*^9, 
  3.766600481247178*^9}},
 CellLabel->
  "In[101]:=",ExpressionUUID->"2f9a8cef-843c-4fe2-80b9-e97521d837ce"],

Cell[BoxData[
 RowBox[{
  RowBox[{"s3", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"2", ",", "3"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "2", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "3", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"4", ",", "3"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "4", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "3", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{"2", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"r", "[", 
        RowBox[{"[", "3", "]"}], "]"}], "-", "g4"}], ")"}], "*", 
     RowBox[{"wVec", "[", 
      RowBox[{"[", "3", "]"}], "]"}]}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766600411947711*^9, 3.766600443721468*^9}},
 CellLabel->
  "In[102]:=",ExpressionUUID->"53666c8c-fb46-484e-be23-8c50d746d1dc"],

Cell[BoxData[
 RowBox[{
  RowBox[{"s4", " ", "=", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"1", ",", "4"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "1", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "4", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{
     RowBox[{"Q4", "[", 
      RowBox[{"[", 
       RowBox[{"3", ",", "4"}], "]"}], "]"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "3", "]"}], "]"}], "^", "2"}], "-", 
       RowBox[{
        RowBox[{"wVec", "[", 
         RowBox[{"[", "4", "]"}], "]"}], "^", "2"}]}], ")"}]}], " ", "+", " ", 
    RowBox[{"2", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"r", "[", 
        RowBox[{"[", "4", "]"}], "]"}], "-", "g4"}], ")"}], "*", 
     RowBox[{"wVec", "[", 
      RowBox[{"[", "4", "]"}], "]"}]}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766600450461507*^9, 3.766600489942967*^9}},
 CellLabel->
  "In[103]:=",ExpressionUUID->"60cd2514-dee5-4179-81f7-ea78671bdfc2"],

Cell[BoxData[
 RowBox[{
  RowBox[{"sVec", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{"s1", ",", "s2", ",", "s3", ",", "s4"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.766600510118795*^9, 3.7666005178272886`*^9}},
 CellLabel->
  "In[104]:=",ExpressionUUID->"a40d4302-426d-4e33-8ed0-93733df7984d"],

Cell[BoxData[
 RowBox[{
  RowBox[{"gammaVar", " ", "=", " ", 
   RowBox[{"ssRaw", ".", "sVec"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7666005280261364`*^9, 3.766600553151595*^9}, 
   3.7666006873720584`*^9, {3.766601049972251*^9, 3.7666010513904476`*^9}, {
   3.7668378943493185`*^9, 3.766837894586684*^9}, {3.7731517270745792`*^9, 
   3.773151734127736*^9}},
 CellLabel->
  "In[105]:=",ExpressionUUID->"1fd77435-91b8-408c-b024-5186bd73b771"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{"gammaVar", "[", 
   RowBox[{"[", "3", "]"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.767304598444868*^9, 3.7673046200835867`*^9}, {
  3.7728906278804293`*^9, 3.772890627946254*^9}, {3.7731518354908733`*^9, 
  3.7731518464056697`*^9}, {3.773152431046709*^9, 3.7731524314765253`*^9}},
 CellLabel->
  "In[110]:=",ExpressionUUID->"3f7070b9-5476-47a3-a363-9878b0af2ef3"],

Cell[BoxData[
 FractionBox[
  RowBox[{"c", " ", "koff", " ", "kp", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"c", "+", "koff"}], ")"}], "2"]}], "+", 
     RowBox[{"kp", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"1", "+", "kp"}], ")"}], "2"]}]}], ")"}]}], 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"c", "+", "koff"}], ")"}], "3"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "+", "kp"}], ")"}], "3"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "+", "c", "+", "koff", "+", "kp"}], ")"}], 
    "2"]}]]], "Output",
 CellChangeTimes->{{3.767304602349674*^9, 3.7673046222690077`*^9}, {
   3.772890621133462*^9, 3.772890630060603*^9}, 3.7731517771727295`*^9, 
   3.77315184704699*^9, {3.77315241279545*^9, 3.773152433321594*^9}},
 CellLabel->
  "Out[110]=",ExpressionUUID->"c49c2088-5a30-48c7-af2e-2035e2fbaab7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SNR", " ", "=", "  ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"ssRaw", " ", "[", 
     RowBox[{"[", "3", "]"}], "]"}], " ", "/", 
    RowBox[{"gammaVar", "[", 
     RowBox[{"[", "3", "]"}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.772890672544628*^9, 3.772890733777999*^9}, {
   3.7731518868765135`*^9, 3.77315189165275*^9}, {3.773152091985833*^9, 
   3.7731521004681625`*^9}, 3.773152447353093*^9},
 CellLabel->
  "In[111]:=",ExpressionUUID->"f07033e2-1a81-460a-b959-2d9cfe243ff2"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"c", "+", "koff"}], ")"}], "2"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "+", "kp"}], ")"}], "2"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "+", "c", "+", "koff", "+", "kp"}], ")"}], "2"]}], 
  RowBox[{"koff", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"c", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"c", "+", "koff"}], ")"}], "2"]}], "+", 
     RowBox[{"kp", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"1", "+", "kp"}], ")"}], "2"]}]}], ")"}]}]]], "Output",
 CellChangeTimes->{3.7728907379648094`*^9, 3.773151811205775*^9, 
  3.7731518934629126`*^9, 3.773152100993758*^9, 3.773152412938069*^9, 
  3.7731524484621305`*^9},
 CellLabel->
  "Out[111]=",ExpressionUUID->"4c06470a-3ab2-4e92-954b-24084c16dcb3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{"SNR", ",", 
   RowBox[{"koff", "\[Rule]", "0"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7731520087847795`*^9, 3.773152019722517*^9}, {
  3.7731521053760843`*^9, 3.7731521057072287`*^9}},
 CellLabel->
  "In[112]:=",ExpressionUUID->"d5badc1c-3c3f-4217-9e9a-3383dc9dfa65"],

Cell[BoxData["Indeterminate"], "Output",
 CellChangeTimes->{3.7731520203618093`*^9, 3.7731521062009125`*^9, 
  3.7731524131335435`*^9, 3.77315245939083*^9},
 CellLabel->
  "Out[112]=",ExpressionUUID->"21267e17-d38b-4387-ae81-6cd249894df3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{"SNR", ",", 
   RowBox[{"koff", "\[Rule]", "Infinity"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.773152028424262*^9, 3.7731520297511497`*^9}, {
  3.7731521105841646`*^9, 3.773152110973156*^9}, {3.7731521685812044`*^9, 
  3.773152178227392*^9}},
 CellLabel->
  "In[113]:=",ExpressionUUID->"85075a78-6c5f-4494-8729-b7e555a7f631"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"(", 
    RowBox[{"1", "+", 
     RowBox[{"2", " ", "kp"}], "+", 
     SuperscriptBox["kp", "2"]}], ")"}], " ", "\[Infinity]"}], "c"]], "Output",
 CellChangeTimes->{3.773152030383424*^9, 3.773152111446864*^9, 
  3.7731524131654944`*^9, 3.773152466295377*^9},
 CellLabel->
  "Out[113]=",ExpressionUUID->"f284110e-a566-4527-a452-b572dd60690d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{
   RowBox[{"ssRaw", "[", 
    RowBox[{"[", "3", "]"}], "]"}], ",", "c"}], "]"}]], "Input",
 CellChangeTimes->{{3.773152667824931*^9, 3.7731526772397394`*^9}},
 CellLabel->
  "In[114]:=",ExpressionUUID->"2fab48ca-61e7-4959-baa8-24d6bcc68619"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   FractionBox[
    RowBox[{"c", " ", "kp", " ", 
     RowBox[{"(", 
      RowBox[{"1", "+", "kp"}], ")"}]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"c", "+", "koff", "+", 
       RowBox[{"c", " ", "kp"}], "+", 
       RowBox[{"koff", " ", "kp"}]}], ")"}], "2"]]}], "+", 
  FractionBox["kp", 
   RowBox[{"c", "+", "koff", "+", 
    RowBox[{"c", " ", "kp"}], "+", 
    RowBox[{"koff", " ", "kp"}]}]]}]], "Output",
 CellChangeTimes->{3.7731526777802896`*^9},
 CellLabel->
  "Out[114]=",ExpressionUUID->"5f0476fa-ecec-49f7-8114-11dcebe339ff"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1280, 677},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 255, 6, 67, "Section",ExpressionUUID->"0cd71903-4765-4253-86c7-583733eb7283"],
Cell[838, 30, 222, 3, 28, "Input",ExpressionUUID->"f53ddaf6-0803-4542-a92f-8240a1d82c27"],
Cell[1063, 35, 417, 10, 28, "Input",ExpressionUUID->"7d9e31ed-9dd1-483b-a957-1f1d4020b646"],
Cell[1483, 47, 1601, 34, 86, "Input",ExpressionUUID->"9c208344-b914-4a91-af3b-57aa27d1e452"],
Cell[CellGroupData[{
Cell[3109, 85, 164, 3, 28, "Input",ExpressionUUID->"c1da6231-a226-4878-886a-1ae8cbefdc95"],
Cell[3276, 90, 1035, 31, 96, "Output",ExpressionUUID->"072ec8d5-a714-41df-b696-0637eb85c3c6"]
}, Open  ]],
Cell[4326, 124, 365, 8, 28, "Input",ExpressionUUID->"59cb4cf4-27f2-4409-937f-21b6551d42d3"],
Cell[4694, 134, 276, 5, 28, "Input",ExpressionUUID->"5d25927e-1ef7-47f6-a1dc-8569ff8b995e"],
Cell[CellGroupData[{
Cell[4995, 143, 523, 11, 28, "Input",ExpressionUUID->"1b51bc59-d5a1-412b-9f72-d20ee10c2b54"],
Cell[5521, 156, 801, 22, 59, "Output",ExpressionUUID->"0d16e703-a497-4eb9-aa6e-b398f31a8cae"]
}, Open  ]],
Cell[6337, 181, 247, 3, 28, "Input",ExpressionUUID->"4fe61074-1312-425c-ae10-f001d7295dbd"],
Cell[6587, 186, 290, 6, 28, "Input",ExpressionUUID->"fd7c28c3-652c-4b27-b207-09b80f324c5f"],
Cell[6880, 194, 253, 5, 28, "Input",ExpressionUUID->"dd6edc2a-abb3-4355-b2b7-a9b600e99bc1"],
Cell[CellGroupData[{
Cell[7158, 203, 212, 4, 44, "Subsubsection",ExpressionUUID->"4bbb22a6-f9b4-4c69-8d47-32491673556f"],
Cell[7373, 209, 352, 7, 28, "Input",ExpressionUUID->"ca8e9be0-0b84-4e9d-9416-0b70501605ff"],
Cell[7728, 218, 782, 22, 28, "Input",ExpressionUUID->"627ec138-4cf3-45df-95dd-9dca55ad5796"],
Cell[8513, 242, 782, 22, 28, "Input",ExpressionUUID->"f498e0d9-a95c-4e8d-bc15-854e2613ed30"],
Cell[9298, 266, 731, 21, 28, "Input",ExpressionUUID->"bf44010f-bcbe-4e20-b538-313668dbb678"],
Cell[10032, 289, 731, 21, 28, "Input",ExpressionUUID->"fa518dc3-7d43-4e49-af75-3f71a5204214"],
Cell[10766, 312, 519, 9, 28, "Input",ExpressionUUID->"e379dcb2-a5ad-40f2-9be5-267b2ed7185a"],
Cell[11288, 323, 464, 12, 28, "Input",ExpressionUUID->"2c6f4674-9887-4b04-8adc-f8feb90a44c3"],
Cell[11755, 337, 440, 10, 28, "Input",ExpressionUUID->"77bcbbd5-e42c-4088-8b3e-43edf1ecbb7a"],
Cell[12198, 349, 1487, 41, 28, "Input",ExpressionUUID->"0e464976-b146-49e2-9878-98e36c496bc6"],
Cell[13688, 392, 1303, 39, 28, "Input",ExpressionUUID->"2f9a8cef-843c-4fe2-80b9-e97521d837ce"],
Cell[14994, 433, 1203, 37, 28, "Input",ExpressionUUID->"53666c8c-fb46-484e-be23-8c50d746d1dc"],
Cell[16200, 472, 1203, 37, 28, "Input",ExpressionUUID->"60cd2514-dee5-4179-81f7-ea78671bdfc2"],
Cell[17406, 511, 304, 7, 28, "Input",ExpressionUUID->"a40d4302-426d-4e33-8ed0-93733df7984d"],
Cell[17713, 520, 446, 9, 28, "Input",ExpressionUUID->"1fd77435-91b8-408c-b024-5186bd73b771"],
Cell[CellGroupData[{
Cell[18184, 533, 427, 8, 28, "Input",ExpressionUUID->"3f7070b9-5476-47a3-a363-9878b0af2ef3"],
Cell[18614, 543, 960, 28, 63, "Output",ExpressionUUID->"c49c2088-5a30-48c7-af2e-2035e2fbaab7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19611, 576, 532, 12, 28, "Input",ExpressionUUID->"f07033e2-1a81-460a-b959-2d9cfe243ff2"],
Cell[20146, 590, 886, 27, 63, "Output",ExpressionUUID->"4c06470a-3ab2-4e92-954b-24084c16dcb3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21069, 622, 320, 7, 28, "Input",ExpressionUUID->"d5badc1c-3c3f-4217-9e9a-3383dc9dfa65"],
Cell[21392, 631, 239, 4, 32, "Output",ExpressionUUID->"21267e17-d38b-4387-ae81-6cd249894df3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21668, 640, 376, 8, 28, "Input",ExpressionUUID->"85075a78-6c5f-4494-8729-b7e555a7f631"],
Cell[22047, 650, 391, 10, 58, "Output",ExpressionUUID->"f284110e-a566-4527-a452-b572dd60690d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22475, 665, 286, 7, 28, "Input",ExpressionUUID->"2fab48ca-61e7-4959-baa8-24d6bcc68619"],
Cell[22764, 674, 594, 18, 94, "Output",ExpressionUUID->"5f0476fa-ecec-49f7-8114-11dcebe339ff"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

