(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     18929,        494]
NotebookOptionsPosition[     16485,        445]
NotebookOutlinePosition[     16828,        460]
CellTagsIndexPosition[     16785,        457]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
Define Transition matrix and calculate steady state occupancies\
\>", "Section",
 CellChangeTimes->{{3.788792083645147*^9, 
  3.788792098405744*^9}},ExpressionUUID->"ee51803b-3048-4b2c-b805-\
ec333a335d81"],

Cell[BoxData[
 RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}]], "Input",
 CellLabel->
  "In[127]:=",ExpressionUUID->"f5b84859-dab0-4046-9715-d89e75361b8a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Qfreq", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "r1"}], "-", 
        RowBox[{"c", "*", "k4"}]}], ",", "          ", "k1", "         ", ",",
        "       ", "0", ",", "               ", "r4"}], "}"}], ",", 
     "\[IndentingNewLine]", "               ", 
     RowBox[{"{", 
      RowBox[{"r1", ",", "                       ", 
       RowBox[{
        RowBox[{"-", "k1"}], "-", 
        RowBox[{"c", "*", "r2"}]}], ",", "      ", "k2", ",", "          ", 
       "0"}], "}"}], ",", "\[IndentingNewLine]", "               ", 
     RowBox[{"{", 
      RowBox[{"0", ",", "                               ", 
       RowBox[{"c", "*", "r2"}], ",", "        ", 
       RowBox[{
        RowBox[{"-", "k2"}], "-", "r3"}], ",", "  ", "k3"}], "}"}], ",", 
     "\[IndentingNewLine]", "             ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"c", "*", "k4"}], ",", "                           ", "0", ",",
        "           ", "r3", ",", "            ", 
       RowBox[{
        RowBox[{"-", "k3"}], "-", "r4"}]}], "}"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.788457944678963*^9, 3.7884580197440615`*^9}, {
  3.7884580570277863`*^9, 3.7884581499449034`*^9}, {3.7884585429740343`*^9, 
  3.7884585473034983`*^9}, {3.7884585926442175`*^9, 3.7884586270192347`*^9}, {
  3.7884653952017164`*^9, 3.7884653983353357`*^9}, {3.7884656270097136`*^9, 
  3.788465634597386*^9}, {3.788792009498432*^9, 3.788792009649071*^9}, {
  3.7887920601951103`*^9, 3.788792060472347*^9}, {3.7887979770705814`*^9, 
  3.788797997751046*^9}, {3.7901123880768237`*^9, 3.7901124054269085`*^9}},
 CellLabel->
  "In[128]:=",ExpressionUUID->"657abe23-4ede-49f1-89c9-1414045ff907"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", "Qfreq", "]"}]], "Input",
 CellChangeTimes->{{3.788458158164879*^9, 3.7884581611768217`*^9}},
 NumberMarks->False,
 CellLabel->
  "In[129]:=",ExpressionUUID->"a1b82b83-4d99-4d7a-975a-f1327e99313a"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       RowBox[{
        RowBox[{"-", "c"}], " ", "k4"}], "-", "r1"}], "k1", "0", "r4"},
     {"r1", 
      RowBox[{
       RowBox[{"-", "k1"}], "-", 
       RowBox[{"c", " ", "r2"}]}], "k2", "0"},
     {"0", 
      RowBox[{"c", " ", "r2"}], 
      RowBox[{
       RowBox[{"-", "k2"}], "-", "r3"}], "k3"},
     {
      RowBox[{"c", " ", "k4"}], "0", "r3", 
      RowBox[{
       RowBox[{"-", "k3"}], "-", "r4"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.788465303714238*^9, 3.788465368513846*^9, 3.78846541101143*^9, 
   3.788465661182635*^9, 3.788560888710956*^9, 3.788561249188279*^9, 
   3.7885613612738867`*^9, 3.7885621417528906`*^9, 3.7886211768015385`*^9, 
   3.788621300179056*^9, 3.788650026361615*^9, 3.7886500942238474`*^9, 
   3.7886510416519513`*^9, 3.788652255809455*^9, 3.788712638525761*^9, 
   3.788713192748004*^9, 3.788792360345148*^9, 3.7887980027486296`*^9, 
   3.7888060607527914`*^9, 3.78880613929148*^9, 3.788807524630579*^9, 
   3.788807584506338*^9, 3.788807655835704*^9, 3.788808006388104*^9, 
   3.788808183037671*^9, 3.7888084639807777`*^9, 3.7888091261079493`*^9, 
   3.7901123788088627`*^9, 3.7901124088150444`*^9, {3.798897541712846*^9, 
   3.79889756228148*^9}},
 CellLabel->
  "Out[129]//MatrixForm=",ExpressionUUID->"b1b1d5c2-b0ac-4bdf-a61d-\
70b183c415a3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Total", "[", "Qfreq", "]"}]], "Input",
 CellChangeTimes->{{3.7884581648041186`*^9, 3.7884581685590754`*^9}},
 CellLabel->
  "In[130]:=",ExpressionUUID->"76234983-9250-44fe-9a51-48ddafcfae0d"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{
  3.78845816888221*^9, 3.7884587070828986`*^9, 3.7884587745694275`*^9, 
   3.788462387399745*^9, 3.7884643172660847`*^9, 3.788464461311801*^9, {
   3.7884647681148834`*^9, 3.7884647872088404`*^9}, 3.788465303802931*^9, 
   3.788465368624548*^9, 3.788465661223525*^9, 3.7885608887651267`*^9, 
   3.788561249208509*^9, 3.788561361295083*^9, 3.7885621417739763`*^9, 
   3.788621176818489*^9, 3.7886213001990027`*^9, 3.7886500264231396`*^9, 
   3.7886500943142157`*^9, 3.788651041690335*^9, 3.7886522558569455`*^9, 
   3.788712638634693*^9, 3.7887131928655443`*^9, 3.788792362255617*^9, 
   3.7887980053786273`*^9, 3.7888060607757607`*^9, 3.7888061393306046`*^9, 
   3.78880752466748*^9, 3.7888075845412955`*^9, 3.788807655859697*^9, 
   3.7888080064200506`*^9, 3.7888081830785584`*^9, 3.7888084640146875`*^9, 
   3.7888091261588135`*^9, {3.7901123788434906`*^9, 3.7901124088429317`*^9}, {
   3.798897541752102*^9, 3.7988975623170724`*^9}},
 CellLabel->
  "Out[130]=",ExpressionUUID->"29726f0b-5943-4301-94ef-fc19a76f6390"]
}, Open  ]],

Cell["\<\
Impose simplifying assumptions--we are taking the infinite-drive limit here\
\>", "Text",
 CellChangeTimes->{{3.7887921285895042`*^9, 
  3.7887921533951087`*^9}},ExpressionUUID->"584cbc0b-ce70-4ee7-9e6a-\
5eee1493a512"],

Cell[BoxData[
 RowBox[{
  RowBox[{"eigVectorsFreq", " ", "=", " ", 
   RowBox[{"Eigenvectors", "[", 
    RowBox[{"Qfreq", "/.", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"k1", "\[Rule]", "k"}], ",", 
       RowBox[{"k3", "\[Rule]", "k"}], ",", 
       RowBox[{"k2", "\[Rule]", "k"}], ",", 
       RowBox[{"r1", "\[Rule]", "r"}], ",", 
       RowBox[{"r3", "\[Rule]", "r"}], ",", 
       RowBox[{"r4", "\[Rule]", "r"}]}], "}"}]}], "]"}]}], " ", 
  ";"}]], "Input",
 CellChangeTimes->{
  3.788465779965041*^9, {3.788561218517846*^9, 3.7885612206733747`*^9}, 
   3.7885612691603575`*^9, {3.7886198867831726`*^9, 3.7886199032487297`*^9}, {
   3.788620928412632*^9, 3.7886209303267517`*^9}, {3.7886209885653954`*^9, 
   3.7886210221336827`*^9}, {3.788649990974513*^9, 3.7886500102126017`*^9}, {
   3.788650558872695*^9, 3.788650566979364*^9}, {3.788713128769332*^9, 
   3.7887131291907463`*^9}, {3.7887980277278576`*^9, 
   3.7887980345392427`*^9}, {3.7901123449797554`*^9, 3.7901123652865963`*^9}},
 CellLabel->
  "In[131]:=",ExpressionUUID->"f682bebd-f780-4b31-8058-45a52195f50e"],

Cell[BoxData[
 RowBox[{
  RowBox[{"ssVecFreq", " ", "=", " ", 
   RowBox[{
    RowBox[{"eigVectorsFreq", "[", 
     RowBox[{"[", "1", "]"}], "]"}], " ", "/", " ", 
    RowBox[{"Total", "[", 
     RowBox[{"eigVectorsFreq", "[", 
      RowBox[{"[", "1", "]"}], "]"}], "]"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.788621169359016*^9, 3.788621172583788*^9}, {
   3.7886500141166816`*^9, 3.7886500188426642`*^9}, 3.7976131279868283`*^9, 
   3.798897559196118*^9},
 CellLabel->
  "In[132]:=",ExpressionUUID->"ba0a4d4f-fc35-403f-96bc-e6349f0ce627"],

Cell["\<\
Extract expression for state 4 occupancy (taken to be active state)\
\>", "Text",
 CellChangeTimes->{{3.788792230610299*^9, 
  3.788792244949524*^9}},ExpressionUUID->"05ce9e70-b4f0-49bf-a9e3-\
cf3474446a6f"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"p4", " ", "=", " ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{"ssVecFreq", "[", 
    RowBox[{"[", "4", "]"}], "]"}], " ", "]"}]}]], "Input",
 CellChangeTimes->{{3.788458206125594*^9, 3.7884582587299223`*^9}, {
   3.788462524977764*^9, 3.7884625395358105`*^9}, {3.7884625890813017`*^9, 
   3.7884626000609617`*^9}, {3.7884642629204836`*^9, 3.7884643113689184`*^9}, 
   3.788464756292505*^9, {3.7884652970559773`*^9, 3.788465298172989*^9}, {
   3.7884653507663164`*^9, 3.7884653510156493`*^9}, {3.7884656480085135`*^9, 
   3.78846565384925*^9}, {3.788465787576681*^9, 3.788465792760815*^9}, {
   3.7886210423924513`*^9, 3.7886210532494802`*^9}, {3.788621161395314*^9, 
   3.7886211669614563`*^9}, {3.790112414526984*^9, 3.7901124201448774`*^9}, {
   3.7901126967343297`*^9, 3.7901127081281304`*^9}, {3.7901127726925697`*^9, 
   3.7901127911691656`*^9}, {3.7901128542516136`*^9, 3.7901128551536098`*^9}},
 CellLabel->
  "In[133]:=",ExpressionUUID->"0f7cf6c4-b8f9-46ae-b1a7-76acb5e51ef4"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"c", " ", "k", " ", "k4", " ", 
    RowBox[{"(", 
     RowBox[{"k", "+", "r"}], ")"}]}], "+", 
   RowBox[{"c", " ", "r", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"c", " ", "k4"}], "+", "r"}], ")"}], " ", "r2"}]}], 
  RowBox[{
   SuperscriptBox["k", "3"], "+", 
   RowBox[{"3", " ", "c", " ", 
    SuperscriptBox["k", "2"], " ", "k4"}], "+", 
   RowBox[{"2", " ", 
    SuperscriptBox["k", "2"], " ", "r"}], "+", 
   RowBox[{"c", " ", "k", " ", "k4", " ", "r"}], "+", 
   RowBox[{"2", " ", "k", " ", 
    SuperscriptBox["r", "2"]}], "+", 
   SuperscriptBox["r", "3"], "+", 
   RowBox[{"c", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"c", " ", "k4", " ", 
       RowBox[{"(", 
        RowBox[{"k", "+", "r"}], ")"}]}], "+", 
      RowBox[{"r", " ", 
       RowBox[{"(", 
        RowBox[{"k", "+", 
         RowBox[{"3", " ", "r"}]}], ")"}]}]}], ")"}], " ", 
    "r2"}]}]]], "Output",
 CellChangeTimes->{
  3.78846579307218*^9, 3.7885608928786755`*^9, 3.788561249931959*^9, 
   3.788561361765583*^9, 3.7885621419079328`*^9, {3.788619893948892*^9, 
   3.788619907646099*^9}, 3.788620936386657*^9, 3.78862100488104*^9, {
   3.7886210366947365`*^9, 3.7886210535881357`*^9}, 3.7886211769201536`*^9, 
   3.788621300313719*^9, 3.788650026749052*^9, 3.7886500954502206`*^9, 
   3.7886510418749275`*^9, 3.7886522561061163`*^9, 3.7887126389722533`*^9, 
   3.7887131933424816`*^9, 3.788792369411908*^9, 3.7887980428564696`*^9, 
   3.788806060902509*^9, 3.7888061394813395`*^9, 3.7888075248370266`*^9, 
   3.7888075846210732`*^9, 3.7888076559244757`*^9, 3.78880800656962*^9, 
   3.7888081831802864`*^9, 3.7888084641583376`*^9, 3.7888091264480405`*^9, {
   3.7901123727331223`*^9, 3.7901124205704613`*^9}, 3.790112856391101*^9, {
   3.798897542149798*^9, 3.7988975626048727`*^9}},
 CellLabel->
  "Out[133]=",ExpressionUUID->"41922eec-69a1-4fc5-bd95-163e98bcc800"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"dp4", " ", "=", " ", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{"D", "[", 
    RowBox[{"p4", ",", "c"}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7901124366356735`*^9, 3.7901124473481846`*^9}},
 CellLabel->
  "In[134]:=",ExpressionUUID->"3b84bd1c-1ccd-4ed5-8252-d99098986327"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"k", " ", "k4", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"k", "+", "r"}], ")"}], "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["k", "2"], "+", 
      RowBox[{"k", " ", "r"}], "+", 
      SuperscriptBox["r", "2"]}], ")"}]}], "+", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       SuperscriptBox["c", "2"], " ", 
       SuperscriptBox["k", "2"], " ", 
       SuperscriptBox["k4", "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "k"}], "+", "r"}], ")"}]}], "+", 
      RowBox[{"2", " ", "c", " ", "k4", " ", "r", " ", 
       RowBox[{"(", 
        RowBox[{"k", "+", "r"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["k", "2"], "+", 
         RowBox[{"k", " ", "r"}], "+", 
         SuperscriptBox["r", "2"]}], ")"}]}], "+", 
      RowBox[{
       SuperscriptBox["r", "2"], " ", 
       RowBox[{"(", 
        RowBox[{"k", "+", "r"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["k", "2"], "+", 
         RowBox[{"k", " ", "r"}], "+", 
         SuperscriptBox["r", "2"]}], ")"}]}]}], ")"}], " ", "r2"}], "+", 
   RowBox[{"2", " ", 
    SuperscriptBox["c", "2"], " ", "k4", " ", 
    SuperscriptBox["r", "3"], " ", 
    SuperscriptBox["r2", "2"]}]}], 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{
     SuperscriptBox["k", "3"], "+", 
     RowBox[{"3", " ", "c", " ", 
      SuperscriptBox["k", "2"], " ", "k4"}], "+", 
     RowBox[{"2", " ", 
      SuperscriptBox["k", "2"], " ", "r"}], "+", 
     RowBox[{"c", " ", "k", " ", "k4", " ", "r"}], "+", 
     RowBox[{"2", " ", "k", " ", 
      SuperscriptBox["r", "2"]}], "+", 
     SuperscriptBox["r", "3"], "+", 
     RowBox[{"c", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"c", " ", "k4", " ", 
         RowBox[{"(", 
          RowBox[{"k", "+", "r"}], ")"}]}], "+", 
        RowBox[{"r", " ", 
         RowBox[{"(", 
          RowBox[{"k", "+", 
           RowBox[{"3", " ", "r"}]}], ")"}]}]}], ")"}], " ", "r2"}]}], ")"}], 
   "2"]]], "Output",
 CellChangeTimes->{
  3.7901124481923103`*^9, {3.798897542195757*^9, 3.7988975630278797`*^9}},
 CellLabel->
  "Out[134]=",ExpressionUUID->"4acddb18-74aa-4f4d-ad1a-a44ca5d615ac"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"maxSol", " ", "=", " ", 
  RowBox[{"NMaximize", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"dp4", "/.", 
       RowBox[{"c", "\[Rule]", "1"}]}], ",", 
      RowBox[{"r", ">", "0"}], ",", 
      RowBox[{"k", "\[GreaterEqual]", "0.001"}], ",", 
      RowBox[{"r2", ">", ".01"}], ",", 
      RowBox[{"k4", ">", "0"}], ",", 
      RowBox[{"k4", "\[LessEqual]", "1"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"r", ",", "k", ",", "k4", ",", "r2"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.790112451170685*^9, 3.7901125910119658`*^9}, 
   3.7988975391596212`*^9},
 CellLabel->
  "In[135]:=",ExpressionUUID->"79b0b9a0-0dee-4564-8f42-d0f6267d9eb4"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.4131316311013126`", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"r", "\[Rule]", "0.10277595972564725`"}], ",", 
     RowBox[{"k", "\[Rule]", "0.001`"}], ",", 
     RowBox[{"k4", "\[Rule]", "1.`"}], ",", 
     RowBox[{"r2", "\[Rule]", "0.01`"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.798897542252489*^9, 3.79889756345574*^9}},
 CellLabel->
  "Out[135]=",ExpressionUUID->"4a4702f2-11ae-4461-94cf-45ffc607fb17"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"maxSol", "[", 
  RowBox[{"[", "2", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.790112592942083*^9, 3.7901125945367336`*^9}},
 CellLabel->
  "In[136]:=",ExpressionUUID->"07369fa3-0064-4c0a-9d22-d15af7ccd45e"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"r", "\[Rule]", "0.10277595972564725`"}], ",", 
   RowBox[{"k", "\[Rule]", "0.001`"}], ",", 
   RowBox[{"k4", "\[Rule]", "1.`"}], ",", 
   RowBox[{"r2", "\[Rule]", "0.01`"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7901126155200834`*^9, {3.7988975423030877`*^9, 3.7988975635600595`*^9}},
 CellLabel->
  "Out[136]=",ExpressionUUID->"6bdff7d9-8af5-4a86-b7fa-e59fd550e2ab"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  RowBox[{
   RowBox[{"p4", "/.", 
    RowBox[{"maxSol", "[", 
     RowBox[{"[", "2", "]"}], "]"}]}], "/.", 
   RowBox[{"c", "\[Rule]", "1"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.79011259794729*^9, 3.7901126330592003`*^9}},
 CellLabel->
  "In[137]:=",ExpressionUUID->"c07e7e2a-b4f0-403f-8789-281d21cd0c11"],

Cell[BoxData["0.4816878801836713`"], "Output",
 CellChangeTimes->{{3.7901126055092254`*^9, 3.7901126335314045`*^9}, {
  3.7988975423462863`*^9, 3.7988975636019526`*^9}},
 CellLabel->
  "Out[137]=",ExpressionUUID->"27a51180-2a38-4d8b-962e-0bdc50280525"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1280, 677},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 216, 5, 67, "Section",ExpressionUUID->"ee51803b-3048-4b2c-b805-ec333a335d81"],
Cell[799, 29, 156, 3, 28, "Input",ExpressionUUID->"f5b84859-dab0-4046-9715-d89e75361b8a"],
Cell[958, 34, 1772, 38, 86, "Input",ExpressionUUID->"657abe23-4ede-49f1-89c9-1414045ff907"],
Cell[CellGroupData[{
Cell[2755, 76, 239, 5, 28, "Input",ExpressionUUID->"a1b82b83-4d99-4d7a-975a-f1327e99313a"],
Cell[2997, 83, 1849, 46, 96, "Output",ExpressionUUID->"b1b1d5c2-b0ac-4bdf-a61d-70b183c415a3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4883, 134, 215, 4, 28, "Input",ExpressionUUID->"76234983-9250-44fe-9a51-48ddafcfae0d"],
Cell[5101, 140, 1133, 18, 32, "Output",ExpressionUUID->"29726f0b-5943-4301-94ef-fc19a76f6390"]
}, Open  ]],
Cell[6249, 161, 229, 5, 34, "Text",ExpressionUUID->"584cbc0b-ce70-4ee7-9e6a-5eee1493a512"],
Cell[6481, 168, 1085, 23, 28, "Input",ExpressionUUID->"f682bebd-f780-4b31-8058-45a52195f50e"],
Cell[7569, 193, 546, 13, 28, "Input",ExpressionUUID->"ba0a4d4f-fc35-403f-96bc-e6349f0ce627"],
Cell[8118, 208, 217, 5, 34, "Text",ExpressionUUID->"05ce9e70-b4f0-49bf-a9e3-cf3474446a6f"],
Cell[CellGroupData[{
Cell[8360, 217, 1010, 16, 28, "Input",ExpressionUUID->"0f7cf6c4-b8f9-46ae-b1a7-76acb5e51ef4"],
Cell[9373, 235, 1917, 45, 61, "Output",ExpressionUUID->"41922eec-69a1-4fc5-bd95-163e98bcc800"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11327, 285, 308, 7, 28, "Input",ExpressionUUID->"3b84bd1c-1ccd-4ed5-8252-d99098986327"],
Cell[11638, 294, 2276, 69, 63, "Output",ExpressionUUID->"4acddb18-74aa-4f4d-ad1a-a44ca5d615ac"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13951, 368, 700, 18, 28, "Input",ExpressionUUID->"79b0b9a0-0dee-4564-8f42-d0f6267d9eb4"],
Cell[14654, 388, 468, 11, 32, "Output",ExpressionUUID->"4a4702f2-11ae-4461-94cf-45ffc607fb17"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15159, 404, 233, 5, 28, "Input",ExpressionUUID->"07369fa3-0064-4c0a-9d22-d15af7ccd45e"],
Cell[15395, 411, 425, 10, 32, "Output",ExpressionUUID->"6bdff7d9-8af5-4a86-b7fa-e59fd550e2ab"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15857, 426, 345, 9, 28, "Input",ExpressionUUID->"c07e7e2a-b4f0-403f-8789-281d21cd0c11"],
Cell[16205, 437, 252, 4, 32, "Output",ExpressionUUID->"27a51180-2a38-4d8b-962e-0bdc50280525"]
}, Open  ]]
}, Open  ]]
}
]
*)

